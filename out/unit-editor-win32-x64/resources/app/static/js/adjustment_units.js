/**
 * A list of units selected for evaluating attack against. These include units (as well as buildings and ships) most oftenly met in combat, their abstract types and weights (indicating how often the unit appears).
 */
const adjustment_units = [
    {
        weight: 7,
        name: "Musketeer",
        type: "ranged",
        hp: 150,
        categories: [
            "Unit",
            "Ranged",
            "Military",
            "AbstractInfantry",
            "AbstractHeavyInfantry",
            "AbstractGunpowderTrooper",
            "AbstractCavalryInfantry",
            "AbstractRangedInfantry"
        ]
    }, {
        weight: 4,
        name: "Skirmisher",
        type: "ranged",
        hp: 120,
        categories: [
            "AbstractRangedInfantry",
            "AbstractCavalryInfantry",
            "Ranged",
            "AbstractGunpowderTrooper",
            "Unit",
            "UnitClass",
            "Military",
            "AbstractInfantry"
        ]
    }, {
        weight: 1,
        name: "Strelet",
        type: "ranged",
        hp: 90,
        categories: [
            "AbstractGunpowderTrooper",
            "Ranged",
            "HasBountyValue",
            "AbstractCavalryInfantry",
            "AbstractRangedInfantry",
            "UnitClass",
            "Military",
            "Unit",
            "AbstractInfantry"
        ]
    }, {
        weight: 1,
        name: "Pikeman",
        type: "melee",
        hp: 120,
        categories: [
            "Unit",
            "Hand",
            "UnitClass",
            "Military",
            "AbstractHandInfantry",
            "AbstractCavalryInfantry",
            "AbstractPikeman",
            "AbstractHeavyInfantry",
            "AbstractInfantry"
        ]
    }, {
        weight: 1,
        name: "Native Pikeman",
        type: "melee",
        hp: 180,
        categories: [
            "AbstractInfantry",
            "AbstractNativeWarrior",
            "AbstractHeavyInfantry",
            "UnitClass",
            "Military",
            "Unit",
            "AbstractPikeman",
            "AbstractSiegeTrooper",
            "AbstractCavalryInfantry",
            "AbstractHandInfantry"
        ]
    }, {
        weight: 1,
        name: "Native Rifleman",
        type: "ranged",
        hp: 230,
        categories: [
            "AbstractNativeWarrior",
            "UnitClass",
            "Military",
            "Unit",
            "Ranged",
            "AbstractGunpowderTrooper",
            "AbstractCavalryInfantry",
            "AbstractRangedInfantry"
        ]
    }, {
        weight: 2,
        name: "Coyote Runner",
        type: "melee",
        hp: 150,
        categories: [
            "AbstractCavalryInfantry",
            "AbstractHandInfantry",
            "AbstractCoyoteMan",
            "AbstractLightInfantry",
            "Unit",
            "Military",
            "UnitClass"
        ]
    }, {
        weight: 1,
        name: "Aztec Arrow Knight",
        type: "ranged",
        hp: 150,
        categories: [
            "Ranged",
            "AbstractSiegeTrooper",
            "AbstractCavalryInfantry",
            "AbstractRangedInfantry",
            "Unit",
            "UnitClass",
            "Military",
            "AbstractInfantry",
            "AbstractArcher"
        ]
    }, {
        weight: 2,
        name: "Crossbowman",
        type: "ranged",
        hp: 100,
        categories: [
            "Military",
            "UnitClass",
            "Unit",
            "Ranged",
            "AbstractArcher",
            "AbstractInfantry",
            "AbstractCavalryInfantry",
            "AbstractRangedInfantry"
        ]
    }, {
        weight: 1,
        name: "Rajput",
        type: "melee",
        hp: 150,
        categories: [
            "Unit",
            "Military",
            "UnitClass",
            "AbstractHeavyInfantry",
            "AbstractInfantry",
            "AbstractCavalryInfantry",
            "AbstractHandInfantry",
            "AbstractRajput"
        ]
    }, {
        weight: 1,
        name: "Dopplesoldner",
        type: "melee",
        hp: 230,
        categories: [
            "Unit",
            "Military",
            "UnitClass",
            "AbstractInfantry",
            "AbstractHeavyInfantry",
            "HasBountyValue",
            "AbstractCavalryInfantry",
            "AbstractHandInfantry"
        ]
    }, {
        weight: 1,
        name: "Swiss Pikeman",
        type: "melee",
        hp: 320,
        categories: [
            "Unit",
            "UnitClass",
            "Military",
            "Mercenary",
            "AbstractInfantry",
            "AbstractHeavyInfantry",
            "AbstractPikeman",
            "AbstractCavalryInfantry",
            "AbstractHandInfantry"
        ]
    }, {
        weight: 2,
        name: "Black Rider",
        type: "ranged",
        hp: 520,
        categories: [
            "Mercenary",
            "AbstractCavalry",
            "AbstractLightCavalry",
            "Unit",
            "Military",
            "UnitClass",
            "Ranged",
            "AbstractCavalryInfantry",
            "AbstractGunpowderCavalry",
            "AbstractRangedCavalry"
        ]
    }, {
        weight: 2,
        name: "Jaeger",
        type: "ranged",
        hp: 250,
        categories: [
            "AbstractRangedInfantry",
            "AbstractCavalryInfantry",
            "Ranged",
            "AbstractGunpowderTrooper",
            "UnitClass",
            "Military",
            "Unit",
            "AbstractInfantry",
            "Mercenary"
        ]
    }, {
        weight: 3,
        name: "Hussar",
        type: "melee",
        hp: 320,
        categories: [
            "Unit",
            "UnitClass",
            "Military",
            "AbstractCavalry",
            "AbstractHandCavalry",
            "AbstractCavalryInfantry",
            "AbstractHeavyCavalry",
        ]
    }, {
        weight: 1,
        name: "Lancer",
        type: "melee",
        hp: 350,
        categories: [
            "AbstractCavalry",
            "UnitClass",
            "Military",
            "Unit",
            "AbstractLancer",
            "AbstractHeavyCavalry",
            "AbstractCavalryInfantry",
            "AbstractHandCavalry",
        ]
    }, {
        weight: 2,
        name: "Dragoon",
        type: "ranged",
        hp: 200,
        categories: [
            "Unit",
            "Ranged",
            "UnitClass",
            "Military",
            "AbstractRangedCavalry",
            "AbstractGunpowderCavalry",
            "AbstractCavalryInfantry",
            "AbstractCavalry",
            "AbstractLightCavalry"
        ]
    }, {
        weight: 1,
        name: "Native Horse Archer",
        type: "ranged",
        hp: 200,
        categories: [
            "UnitClass",
            "Military",
            "Unit",
            "Ranged",
            "AbstractArcher",
            "AbstractLightCavalry",
            "AbstractCavalry",
            "AbstractNativeWarrior",
            "AbstractRangedCavalry",
            "AbstractCavalryInfantry",
        ]
    }, {
        weight: 1,
        name: "Consulate Uhlan",
        type: "melee",
        hp: 200,
        categories: [
            "AbstractHeavyCavalry",
            "AbstractCavalryInfantry",
            "AbstractHandCavalry",
            "AbstractConsulateUnit",
            "UnitClass",
            "Military",
            "Unit",
            "AbstractCavalry"
        ]
    }, {
        weight: 2,
        name: "Consulate Grenadier",
        type: "melee",
        hp: 200,
        categories: [
            "Ranged",
            "AbstractSiegeTrooper",
            "AbstractCavalryInfantry",
            "AbstractRangedInfantry",
            "AbstractConsulateUnit",
            "AbstractConsulateUnitColonial",
            "AbstractInfantry",
            "AbstractHeavyInfantry",
            "UnitClass",
            "Military",
            "Unit"
        ]
    }, {
        weight: 3,
        name: "Defensive Building",
        type: "building",
        hp: 3000,
        categories: [
            "Building",
            "BuildingClass",
            "MilitaryBuilding",
            "HasBountyValue",
            "AbstractFort",
            "CountsTowardMilitaryScore"
        ]
    }, {
        weight: 3,
        name: "Military Building",
        type: "building",
        hp: 2000,
        categories: [
            "Building",
            "BuildingClass",
            "MilitaryBuilding",
            "AbstractBarracks2",
            "HasBountyValue"
        ]
    }, {
        weight: 2,
        name: "Civic Building",
        type: "building",
        hp: 1500,
        categories: [
            "Building",
            "BuildingClass",
            "Economic",
            "HasBountyValue"
        ]
    }, {
        weight: 3,
        name: "Falconet",
        type: "ranged",
        hp: 200,
        categories: [
            "Ranged",
            "Unit",
            "UnitClass",
            "Military",
            "AbstractArtillery"
        ]
    }, {
        weight: 1,
        name: "Culverin",
        type: "ranged",
        hp: 280,
        categories: [
            "AbstractArtillery",
            "Military",
            "UnitClass",
            "Unit",
            "Ranged"
        ]
    }, {
        weight: 4,
        name: "Ship",
        type: "building",
        hp: 1500,
        categories: [
            "Ship",
            "Transport",
            "Unit",
            "UnitClass",
            "Military",
            "AbstractWarShip",
            "AbstractSiegeTrooper",
            "Ranged"
        ]
    }, {
        weight: 1,
        name: "Warchief",
        type: "melee",
        hp: 700,
        categories: [
            "AbstractInfantry",
            "Hero",
            "Military",
            "UnitClass",
            "Unit",
            "AbstractCanSeeStealth",
            "AbstractHandInfantry",
            "AbstractCavalryInfantry"
        ]
    }, {
        weight: 1,
        name: "Daimyo",
        type: "melee",
        hp: 800,
        categories: [
            "AbstractHandCavalry",
            "AbstractDaimyo",
            "AbstractHeavyCavalry",
            "AbstractCavalryInfantry",
            "Unit",
            "UnitClass",
            "Military",
            "AbstractCavalry"
        ]
    }, {
        weight: 6,
        name: "Villager",
        type: "ranged",
        hp: 150,
        categories: [
            "Unit",
            "UnitClass",
            "Economic",
            "AbstractVillager",
            "Ranged"
        ]
    }, {
        weight: 2,
        name: "Wolf",
        type: "melee",
        hp: 115,
        categories: [
            "NatureClass",
            "AnimalPrey",
            "Unit",
            "Military",
            "Guardian"
        ]
    }, {
        weight: 1,
        name: "Outlaw Rifleman",
        type: "ranged",
        hp: 270,
        categories: [
            "Ranged",
            "Guardian",
            "Military",
            "UnitClass",
            "Unit"
        ]
    }
];
