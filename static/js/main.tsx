import { app, OpenDialogOptions } from 'electron';
const {dialog} = require('electron').remote;
import * as $ from 'jquery';
import * as fs from 'fs';

/**
 * Importing CodeMirror as a Node.js module doesn't work for some reason, so we declare it here for TS and attach to the HTML.
 */
declare namespace CodeMirror {
    function fromTextArea(place: HTMLElement, options: Object): any;
    interface Editor {
        setValue(content: string): void;
    }
};

/**
 * A list of units selected for evaluating attack against. These include units (as well as buildings and ships) most oftenly met in combat, their abstract types and weights (indicating how often the unit appears).
 */
declare const adjustment_units: AdjustmentUnit[];

/**
 * Used to convert TSX tags to document.createElement() function calls similar to how React does it.
 * Is usable for XML as well provided you delete the `xmlns` attributes later.
 */
class TSXtoHTMLEngine {
    /**
     * React-like createElement function so we can use JSX in our TypeScript/JavaScript code.
     */
    public createElement (tag: string, attrs: any, children: any): HTMLElement {
        var element: HTMLElement = document.createElement(tag);
        for (let name in attrs) {
            if (name && attrs.hasOwnProperty(name)) {
                var value: string | null | boolean = attrs[name];
                if (value === true) {
                    element.setAttribute(name, name);
                } else if (value !== false && value != null) {
                    element.setAttribute(name, value.toString());
                }
            }
        }
        for (let i:number = 2; i < arguments.length; i++) {
            let child:any = arguments[i];
            element.appendChild(
                child.nodeType == null ?
                    document.createTextNode(child.toString()) : child);
        }
        return element;
    }
}

const TSXtoHTML:TSXtoHTMLEngine = new TSXtoHTMLEngine();

/**
 * A generator function that applies a function to each element of an array. 
 * Like a `map`, but returns a copy of the original array.
 * @param array An array to apply the function to.
 * @param func A function to apply to elements.
 */
function gen<T>(array: T[], func: (arg: T) => T): T[]  {
    let result: T[] = [];
    array.forEach(element => {
        result.push(func(element));
    });
    return result;
}

/**
 * XML data file loaded by the user.
 */
var $proto: JQuery<XMLDocument>, $techs: JQuery<XMLDocument>, $strings: JQuery<XMLDocument>;
var current_unit: Unit;
var max_unit_id:number, max_unit_dbid:number, max_string_id:number, max_tech_dbid:number;
var string_id_list: number[] = [];
var stringtable_encoding = "utf-8";

/**
 * Asynchronously opens and loads a proto XML file.
 * @param path Path to `protoy.xml` (or other proto file) to load. Path specified in the preferences is used by default.
 */
async function loadProto(path:fs.PathLike = preferences["proto-file"]) {
    let new_proto: XMLDocument;
    try {
        new_proto = $.parseXML( fs.readFileSync(path, 'utf8') );
        if(new_proto.getElementsByTagName("proto").length > 0)
            $proto = $( new_proto );
    } catch (err) {
        console.error(err);
        $("#files-settings-container").css("display", "flex");
    }
}
/**
 * Asynchronously opens and loads a techtree XML file.
 * @param path Path to `techtreey.xml` (or other techtree file) to load. Path specified in the preferences is used by default.
 */
async function loadTechs(path:fs.PathLike = preferences["techtree-file"]) {
    let new_techs: XMLDocument;
    try {
        new_techs = $.parseXML( fs.readFileSync(path, 'utf8') );
        if(new_techs.getElementsByTagName("techtree").length > 0)
            $techs = $( new_techs );
    } catch (err) {
        console.error(err);
        $("#files-settings-container").css("display", "flex");
    }
}
/**
 * Asynchronously opens and loads a stringtable XML file.
 * @param path Path to `stringtabley.xml` (or other stringtable file) to load. Path specified in the preferences is used by default.
 */
async function loadStrings(path:fs.PathLike = preferences["stringtable-file"]) {
    let new_strings: XMLDocument;
    try {
        try {
            new_strings = $.parseXML( fs.readFileSync(path, stringtable_encoding.replace(/-/g, '')) );
        } catch (err) {
            //console.log(`Stringtable error: ${err}`);
            //console.log("Trying UTF-16");
            //stringtable_encoding = "utf-16";
            //new_strings = $.parseXML( fs.readFileSync(path, 'ucs2') );
            console.error(`UTF-16 encoding is not supported.`);
        }
        if(new_strings.getElementsByTagName("stringtable").length > 0)
            $strings = $( new_strings );
        else
            throw new Error(`Stringtable is in an incorrect format: ${new_strings}`);
    } catch (err) {
        console.error(err);
        $("#files-settings-container").css("display", "flex");
    }
}

var preferences_file = window.process.argv.filter( s => s.startsWith("--settings_file=") )[0].slice(16);

/**
 * A string type with four allowed UI themes
 */
type Theme = "ayu-dark"|"light"|"monokai"|"chrome";

/**
 * User preferences as a JSON object.
 */
interface Preferences {
    "theme": Theme,
    "xml-editing": boolean,
    "live-xml": boolean,
    "tech-xml": boolean,
    "new-first": boolean,
    "proto-file": string,
    "techtree-file": string,
    "stringtable-file": string,
    "replace-files": boolean,
    "proto-out": string,
    "techtree-out": string,
    "stringtable-out": string,
}

/**
 * User preferences as a JSON object.
 * Default settings are:
 * ```json
 * preferences = {
 *   "theme": "chrome",
 *   "xml-editing": true,
 *   "live-xml": false,
 *   "tech-xml": true,
 *   "new-first": true,
 *   "proto-file": "protoy.xml",
 *   "techtree-file": "techtreey.xml",
 *   "stringtable-file": "stringtabley.xml",
 *   "replace-files": false,
 *   "proto-out": "protoy_new.xml",
 *   "techtree-out": "techtreey_new.xml",
 *   "stringtable-out": "stringtabley_new.xml"
 * }
 * ```
 */
var preferences:Preferences = {
    "theme":"chrome",
    "xml-editing":true,
    "live-xml":false,
    "tech-xml": true,
    "new-first":true,
    "proto-file": "protoy.xml",
    "techtree-file": "techtreey.xml",
    "stringtable-file": "stringtabley.xml",
    "replace-files": false,
    "proto-out": "protoy_new.xml",
    "techtree-out": "techtreey_new.xml",
    "stringtable-out": "stringtabley_new.xml"
}

/**
 * Save the user preferences JSON to the specified preferences_file.
 * @param new_preferences The preferences to be applies. Uses those set currently by default.
 */
async function savePreferences(new_preferences:Preferences = preferences) {
    fs.writeFile(preferences_file, JSON.stringify({preferences: new_preferences}), (err) => {
        err ? console.log(err) : console.log("Preferences updated successfully");
    });
}
/**
 * Asynchronously load the user preferences from the specified JSON preferences_file to the
 * preferences variable, then apply them to the app.
 * @param callback Function called after user preferences are loaded.
 * Just logs the loaded preferences by default.
 */
async function loadPreferences(callback = () => { console.log(preferences); }) {
    try {
        preferences = JSON.parse(fs.readFileSync(preferences_file, 'utf8')).preferences;
    } catch (err) {
        console.log(err);
    }
    applyPreferences();
    callback();
}
/**
 * Apply the loaded preferences to the page, i. e. set the theme and checks the checkboxes.
 * @param new_preferences The preferences to be applies. Uses those set currently by default.
 */
async function applyPreferences(new_preferences:Preferences = preferences) {
    if (new_preferences["xml-editing"]) {
        $("#xml-editing input").prop("checked", true);
    }
    else {
        $("#live-xml input").prop("disabled", true);
        $("#tech-xml input").prop("disabled", true);
    }
    document.body.className = `theme-${new_preferences.theme}`;
    if (new_preferences["live-xml"])
        $("#live-xml input").prop("checked", true);
    if (new_preferences["tech-xml"])
        $("#tech-xml input").prop("checked", true);
    if (new_preferences["new-first"])
        $("#new-first input").prop("checked", true);
    $("#proto-filepath").val(new_preferences["proto-file"]);
    $("#techs-filepath").val(new_preferences["techtree-file"]);
    $("#strings-filepath").val(new_preferences["stringtable-file"]);
    if (new_preferences["replace-files"]) {
        $("#replace-files input").prop("checked", true);
        $("#proto-filepath-out").val(new_preferences["proto-file"]);
        $("#techs-filepath-out").val(new_preferences["techtree-file"]);
        $("#strings-filepath-out").val(new_preferences["stringtable-file"]);
        ["proto", "techs", "strings"].forEach(file_type => {
            $(`#${file_type}-filepath-out`).prop("disabled", true);
            $(`#${file_type}-selector-out`).prop("disabled", true);
            $(`#${file_type}-filepath-out`).parent().css("opacity", 0.5);
        });
    } else {
        $("#replace-files input").prop("checked", false);
        $("#proto-filepath-out").val(new_preferences["proto-out"]);
        $("#techs-filepath-out").val(new_preferences["techtree-out"]);
        $("#strings-filepath-out").val(new_preferences["stringtable-out"]);
        ["proto", "techs", "strings"].forEach(file_type => {
            $(`#${file_type}-filepath-out`).prop("disabled", false);
            $(`#${file_type}-selector-out`).prop("disabled", false);
            $(`#${file_type}-filepath-out`).parent().css("opacity", 1.0);
        });
    }
}

/**
 * Receives a signal from the Electron menu ribbon to perform actions 
 * outside the scope of the usual actions.
 * @param action_type Type of action to perform: open/save the XML files, toggle XML settings or theme settings.
 */
function menuAction(action_type: "open-xml"|"save-xml"|"preferences-editor"|"preferences-theme") {
    $(".settings-background").css("display", "none");
    switch (action_type) {
        case "open-xml":
            selectMultipleFiles();
            $("#files-settings-container").css("display", "flex");
            break;
        case "save-xml":
            $("#saving-settings-container").css("display", "flex");
            break;
        case "preferences-editor":
            $("#xml-settings-container").css("display", "flex");
            break;
        case "preferences-theme":
            $("#theme-selection-container").css("display", "flex");
            break;    
        default:
            break;
    }
}

/**
 * Get the list of all units, plus find the largest used 
 * unit/string IDs. Turn off the loading animation.
 */
function parseFiles() { 
    console.log("Parsing files");
    $("#base-unit-selection").css("display", "flex");
    $("#unit").css("display", "inherit");
    $("#file-message").css("display", "none");
    if($proto !== undefined)
        getUnitList();
    if($strings !== undefined)
        getStringList();
    if($techs !== undefined)
        getTechList();
    $("#loading-container").css("display", "none");
    $("#save-button").css("display", "flex");
    $("#settings-button").css("display", "flex");
    $("#settings-container").css("display", "flex");

}
/**
 * Asynchronously loads the three .XML files, then, after the proto is loaded,
 * parses the unit list with `parseFiles()`
 */
async function loadFiles() {
    console.log(preferences);
    Promise.all([loadProto(), loadStrings(), loadTechs()]).then(parseFiles);
}
/**
 * Autofix unit ID and DBID to the smalles unused. Update the unit accordingly.
 */
function fixUnitId() {
    $("#unit-id").val(max_unit_id + 1);
    current_unit.setUnitID(max_unit_id + 1);
    current_unit.setUnitDBID(max_unit_dbid + 1);
};

loadPreferences(loadFiles);

require("electron").ipcRenderer.on("menu-action", function(event, action_type) { menuAction(action_type); });
$("#settings-help").on("click", () => { require("electron").shell.openExternal("https://gitlab.com/KEremey/unit-editor/-/blob/master/README.md") });

const xmlParser = new DOMParser();
var xmlDoc = xmlParser.parseFromString("<root></root>", "text/xml");


$(".toggle-option").on("click", function() {
    if(!$(this).find("input[type='checkbox']").attr("disabled"))
        $(this).find("input[type='checkbox']").prop("checked", !$(this).find("input[type='checkbox']").prop("checked"));
});

$("#settings-theme").on("click", function() {
    $("#theme-selection-container").css("display", "flex");
});
$("#settings-xml").on("click", function() {
    $("#live-xml input").prop("disabled", true); // TODO: after live-xml is implemented, delete this
    $("#xml-settings-container").css("display", "flex");
});
$("#settings-files").on("click", function() {
    $("#files-settings-container").css("display", "flex");
});
$("#save-button").on("click", function() {
    $("#saving-settings-container").css("display", "flex");
});
$("#cancel-file-selection").on("click", function() {
    $("#files-settings-container").css("display", "none");
});
$("#cancel-saving").on("click", function() {
    $("#saving-settings-container").css("display", "none");
});
$(".settings-background.close-on-click").on("click", function() {
    $(".settings-background").css("display", "none")
});
$(".settings-background>div").on("click", function (e) {
    let evt = e ? e : window.event;
    if (evt.stopPropagation)
        evt.stopPropagation();
    return false; /* Ignore click - to prevent clicks from registering on lower layers */
});
$("#xml-editing").on("click", function() {
    preferences["xml-editing"] = $("#xml-editing input").prop("checked");
    if(preferences["xml-editing"]) {
        if(current_unit != undefined)
            if(current_unit.unit_xml_editor == undefined)
                current_unit.createXMLEditor();
            else
                current_unit.updateXMLEditor();
        $("#unit-xml").css("display", "block");
        $("#tech-xml input").removeAttr("disabled");
        showHideUpgradesXML();
        //$("#live-xml input").removeAttr("disabled"); // TODO: after live-xml is implemented, enable this
    }
    else {
        if(current_unit != undefined && current_unit.unit_xml_editor != undefined)
            $("#unit-xml").css("display", "none");
        $("#live-xml input").prop("disabled", true);
        $("#tech-xml input").prop("disabled", true);
        showHideUpgradesXML();
    }
    savePreferences();
});
$("#live-xml").on("click", function() {
    $("#live-xml input").prop("disabled", true); // TODO: after live-xml is implemented, delete this
    if($("#live-xml input").prop("disabled"))
        return;
    preferences["live-xml"] = $("#live-xml input").prop("checked");
    savePreferences();
});
$("#tech-xml").on("click", function() {
    if($("#tech-xml input").prop("disabled"))
        return;
    preferences["tech-xml"] = $("#tech-xml input").prop("checked");
    showHideUpgradesXML();
    savePreferences();
});
$("#new-first").on("click", function() {
    preferences["new-first"] = $("#new-first input").prop("checked");
    savePreferences();
});
$("#replace-files").on("click", function() {
    preferences["replace-files"] = $("#replace-files input").prop("checked");
    if (preferences["replace-files"]) {
        $("#proto-filepath-out").val(preferences["proto-file"]);
        $("#techs-filepath-out").val(preferences["techtree-file"]);
        $("#strings-filepath-out").val(preferences["stringtable-file"]);
        ["proto", "techs", "strings"].forEach(file_type => {
            $(`#${file_type}-filepath-out`).prop("disabled", true);
            $(`#${file_type}-selector-out`).prop("disabled", true);
            $(`#${file_type}-filepath-out`).parent().css("opacity", 0.5);
        });
    }
    else {
        $("#proto-filepath-out").val(preferences["proto-out"]);
        $("#techs-filepath-out").val(preferences["techtree-out"]);
        $("#strings-filepath-out").val(preferences["stringtable-out"]);
        ["proto", "techs", "strings"].forEach(file_type => {
            $(`#${file_type}-filepath-out`).prop("disabled", false);
            $(`#${file_type}-selector-out`).prop("disabled", false);
            $(`#${file_type}-filepath-out`).parent().css("opacity", 1.0);
        });
    }
    savePreferences();
});

(["ayu-dark", "light", "monokai", "chrome"] as const).forEach((theme: Theme) => {
    ($(`#theme-list .theme-${theme}`).on("click", function() {
        document.body.className = `theme-${theme}`;
        $("#theme-selection-container")[0].style.display = "none";
        preferences.theme = theme;
        savePreferences();
    }));
});

/**
 * Show tech XML on all upgrades if `preferences["tech-xml"] && preferences["xml-editing"]` are on,
 * or hide them all otherwise.
 */
async function showHideUpgradesXML() {
    if (preferences["tech-xml"] && preferences["xml-editing"]) {
        if (current_unit !== undefined) {
            for (const upgrade_name in current_unit.upgrades) {
                if (Object.prototype.hasOwnProperty.call(current_unit.upgrades, upgrade_name)) {
                    if (current_unit.upgrades[upgrade_name].xml_editor === undefined)
                        current_unit.upgrades[upgrade_name].createXMLEditor();
                    else
                        current_unit.upgrades[upgrade_name].updateXMLEditor();
                    $(`#${upgrade_name}-xml`).css("display", "block");
                }
            }
        }
    }
    else {
        if (current_unit !== undefined)
            for (const upgrade_name in current_unit.upgrades)
                if (Object.prototype.hasOwnProperty.call(current_unit.upgrades, upgrade_name))
                    $(`#${upgrade_name}-xml`).css("display", "none");
    }
}

/**
 * Make `.persistent` tooltips fade a second later rather than immediately.
 * @param inside A specific JQuery selector to limit binding inside.
 */
function bindPersistentTips(inside:string= '') {
    $(inside + " .tooltip-container.persistent").on("mouseenter", function () {
        $(".persistent").next().removeClass("showed");
        $(".persistent").next().removeClass("fade-out");
        $(this).next().addClass("showed");
    });
    $(inside + " .tooltip-container.persistent").on("mouseleave", function () {
        $(this).next().addClass("showed");
        setTimeout(() => {
            $(this).next().addClass("fade-out");
            $(this).next().removeClass("showed");
        }, 500);
        setTimeout(() => {
            $(this).next().removeClass("fade-out");
        }, 600);
    });
    $(inside + " .persistent+.warning-tooltip").on("mouseenter", function () {
        $(".persistent").next().removeClass("showed");
        $(".persistent").next().removeClass("fade-out");
        $(this).addClass("showed");
    });
    $(inside + " .persistent+.warning-tooltip").on("mouseleave", function () {
        $(this).addClass("showed");
        setTimeout(() => {
            $(this).addClass("fade-out");
            $(this).removeClass("showed");
        }, 500);
        setTimeout(() => {
            $(this).removeClass("fade-out");
        }, 600);
    });
}
bindPersistentTips();

$("#fix-unit-id").on("click", fixUnitId);

$("#add-building").on("click", function () {
    current_unit.addEmptyBuilding();
});

var string_id_types: string[] = [];

type StringRecord = Record<string, Set<string>>;
/**
 * A record of all the strings to be added to the stringtable.
 * Here's an example of how it could look:
 * ```javascript
 * new_strings = {
 *     77451: {
 *         "Musketeer": Set(["unit-display-name", "elite-musketeer"]),
 *         "INF Musketeer": Set(["unit-editor-name"]),
 *     }
 * };
 * ```
 */
var new_strings_record: Record<number, StringRecord>;

interface AdjustmentUnit {
    weight: number,
    name: string,
    type: "ranged"|"melee"|"building",
    hp: number,
    categories: string[]
};

/**
 * Run through all the unit names in the proto and replaces the list
 * of unit in the search datalist. Also find the largest used unit ID and DBID.
 * @param proto A proto XML to process. Uses `$proto` by default.
 */
async function getUnitList(proto:JQuery<XMLDocument> = $proto) {
    $("#units").empty();
    max_unit_id = 0;
    max_unit_dbid = 0;
    proto.find("unit").map(function () {
        $("#units")[0].appendChild(<option value={$(this).attr("name")}></option>);
        max_unit_id = max_unit_id > parseInt($(this).attr("id"))? max_unit_id : parseInt($(this).attr("id"));
        max_unit_dbid = max_unit_dbid > parseInt($(this).find("dbid").text())? max_unit_dbid : parseInt($(this).find("dbid").text());
    });
}
/**
 * Run through all the techs to find the largest used tech DBID.
 * Save tech names for the datalist.
 * @param techs A techtable XML to process. Uses `$techs` by default.
 */
async function getTechList(techs:JQuery<XMLDocument> = $techs) {
    $("#techs").empty();
    max_tech_dbid = 0;
    techs.find("tech").map(function () {
        $("#techs")[0].appendChild(<option value={$(this).attr("name")}></option>);
        max_tech_dbid = max_tech_dbid > parseInt($(this).find("dbid").text())? max_tech_dbid : parseInt($(this).find("dbid").text());
    });
}
/**
 * Run through all the strings to find the largest used string ID.
 * Save used IDs to `string_id_list`.
 * @param strings A stringtable XML to process. Uses `$strings` by default.
 */
async function getStringList(strings:JQuery<XMLDocument> = $strings) {
    strings.find("string").map(function () {
        if(this.innerHTML.length > 0) {
            string_id_list.push(parseInt($(this).attr("_locid")));
        }
    });
    max_string_id = Math.max(...string_id_list);
}
/**
 * A non-recursive binary search function.
 * @param array An array to search in.
 * @param x An element to search.
 * @returns The largest index that does not exceed the element.
 */
function binarySearch<T>(array: T[], x: T) {
    let l: number = 0;
    let r: number = array.length - 1;
    let m: number;
    while (l <= r) {
        m = (r + l) >> 1;
        if (x > array[m]) {
            l = m + 1;
        } else if(x < array[m]) {
            r = m - 1;
        } else {
            return m;
        }
    }
    return l - 1;
}

/**
 * Save current unit, upgrades and strings to the .XML files.
 * @param with_preferences User preferencies, including file paths and "new-first" policy. Takes current user preferences by default.
 */
function saveChanges(with_preferences:Preferences = preferences) {
    if(with_preferences["new-first"]) {
        if($proto.find(`unit[name=${current_unit.name}]`)[0] !== undefined)
            $proto.find(`unit[name=${current_unit.name}]`).replaceWith(current_unit.$xml[0]);
        else {
            $proto.find("unit").first().before(current_unit.$xml[0]);
            current_unit.buildings.forEach(building => {
                $proto.find(`unit[name=${building.name}] unittype`).last().after(`\n\t\t<train row="0" page="0" column="${building.column}">${current_unit.name}</train>`);
                for (const upgrade_key in current_unit.upgrades) {
                    if (Object.prototype.hasOwnProperty.call(current_unit.upgrades, upgrade_key)) {
                        const upgrade = current_unit.upgrades[upgrade_key];
                        $proto.find(`unit[name=${building.name}] train`).last().after(`\n\t\t<tech row="0" page="1" column="${building.column}">${upgrade.name}</tech>`);
                    }
                }
            });
            $proto.find("unit").first().after("\n\t");
        }
        for (const upgrade_key in current_unit.upgrades) {
            if (Object.prototype.hasOwnProperty.call(current_unit.upgrades, upgrade_key)) {
                const upgrade = current_unit.upgrades[upgrade_key];
                if($techs.find(`tech[name=${upgrade.name}]`)[0] !== undefined)
                    $techs.find(`tech[name=${upgrade.name}]`).replaceWith(prettyPrintXML(upgrade.$xml[0].outerHTML, 4).replace(/\n/g, "\n    "));
                else
                    $techs.find("tech").first().before(`${prettyPrintXML(upgrade.$xml[0].outerHTML, 4).replace(/\n/g, "\n    ")}\n\t`);
            }
        }
    }
    else {
        if($proto.find(`unit[name=${current_unit.name}]`)[0] !== undefined)
            $proto.find(`unit[name=${current_unit.name}]`).replaceWith(current_unit.$xml[0]);
        else {
            $proto.find("unit").last().after(current_unit.$xml[0]);
            current_unit.buildings.forEach(building => {
                for (const upgrade_key in current_unit.upgrades) {
                    if (Object.prototype.hasOwnProperty.call(current_unit.upgrades, upgrade_key)) {
                        const upgrade = current_unit.upgrades[upgrade_key];
                        $proto.find(`unit[name=${building.name}] flag`).first().before(`\t\t<tech row="0" page="1" column="${building.column}">${upgrade.name}</tech>\n`);
                    }
                }
                $proto.find(`unit[name=${building.name}] tech`).first().before(`\t\t<train row="0" page="0" column="${building.column}">${current_unit.name}</train>\n`);
            });
            $proto.find("unit").last().before("\n\t");
        }
        for (const upgrade_key in current_unit.upgrades) {
            if (Object.prototype.hasOwnProperty.call(current_unit.upgrades, upgrade_key)) {
                const upgrade = current_unit.upgrades[upgrade_key];
                if($techs.find(`tech[name=${upgrade.name}]`)[0] !== undefined)
                    $techs.find(`tech[name=${upgrade.name}]`).replaceWith(prettyPrintXML(upgrade.$xml[0].outerHTML, 4).replace(/\n/g, "\n    "));
                else
                    $techs.find("tech").last().after(`\n\t${prettyPrintXML(upgrade.$xml[0].outerHTML, 4).replace(/\n/g, "\n    ")}`);
            }
        }
    }
    saveProtoFile(with_preferences, $proto[0].documentElement.outerHTML);
    saveTechsFile(with_preferences, $techs[0].documentElement.outerHTML);
    Object.keys(new_strings_record).forEach(function (id: string|number) {
        id = parseInt(id as string);
        const str = Object.keys(new_strings_record[id])[0];
        if($strings.find(`string[_locid=${id}]`)[0] == undefined) {
            let prev_id = string_id_list[binarySearch(string_id_list, id)];
            let new_str = $(xmlDoc.createElement("string"));
            new_str.attr("_locid", id);
            new_str.text(str);
            $strings.find(`string[_locid=${prev_id}]`).last().after(new_str);
            $strings.find(`string[_locid=${id}]`).before("\n\t\t");
        }
        else
            $strings.find(`string[_locid=${id}]`).text(str);
    });
    saveStringtableFile(with_preferences, $strings[0].documentElement.outerHTML);
}
/**
 * Save a proto XML file.
 * @param with_preferences User preferences to save with.
 * @param data Data to be written into the file. Takes the `$proto` by default.
 */
async function saveProtoFile(with_preferences:Preferences = preferences, data:string = $proto[0].documentElement.outerHTML) {
    data = `<?xml version="1.0" encoding="utf-8"?>\n\n${data}`;
    if (with_preferences["replace-files"])
        fs.writeFileSync(with_preferences["proto-file"], data, 'utf-8');
    else
        fs.writeFileSync(with_preferences["proto-out"], data, 'utf-8');
    console.log("Saved proto");
}
/**
 * Save a techtree XML file.
 * @param with_preferences User preferences to save with.
 * @param data Data to be written into the file. Takes the `$tech` by default.
 */
async function saveTechsFile(with_preferences:Preferences = preferences, data:string = $techs[0].documentElement.outerHTML) {
    data = `<?xml version="1.0" encoding="utf-8"?>\n\n${data}`;
    if (with_preferences["replace-files"])
        fs.writeFileSync(with_preferences["techtree-file"], data, 'utf-8');
    else
        fs.writeFileSync(with_preferences["techtree-out"], data, 'utf-8');
    console.log("Saved techs");
}
/**
 * Save a stringtable XML file.
 * @param with_preferences User preferences to save with.
 * @param data Data to be written into the file. Takes the `$strings` by default.
 */
async function saveStringtableFile(with_preferences:Preferences = preferences, data:string = $strings[0].documentElement.outerHTML) {
    data = `<?xml version="1.0" encoding="${stringtable_encoding}"?>\n\n${data}`;
    let encoding = stringtable_encoding;
    if(stringtable_encoding === "utf-16")
        encoding = "UCS-2";
    if (with_preferences["replace-files"])
        fs.writeFileSync(with_preferences["strings-file"], data, encoding);
    else
        fs.writeFileSync(with_preferences["stringtable-out"], data, encoding);
    console.log("Saved strings");
}

/**
 * Pretty-print, i. e. add tabs and new lines to an XML string.
 * @param xml The string to pretty-print.
 * @returns "Prettified" string.
 */
function prettyPrintXML(xml: string, indent: number|string = 2) {
    let xmlDoc = xmlParser.parseFromString(xml, 'application/xml');
    let xsltDoc = xmlParser.parseFromString([
        // describes how we want to modify the XML - indent everything
        '<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">',
        '  <xsl:strip-space elements="*"/>',
        '  <xsl:template match="para[content-style][not(text())]">', // change to just text() to strip space in text nodes
        '    <xsl:value-of select="normalize-space(.)"/>',
        '  </xsl:template>',
        '  <xsl:template match="node()|@*">',
        '    <xsl:copy><xsl:apply-templates select="node()|@*"/></xsl:copy>',
        '  </xsl:template>',
        '  <xsl:output indent="yes"/>',
        '</xsl:stylesheet>',
    ].join('\n'), 'application/xml');

    let xsltProcessor = new XSLTProcessor();    
    xsltProcessor.importStylesheet(xsltDoc);
    let resultDoc = xsltProcessor.transformToDocument(xmlDoc);
    let resultXml = new XMLSerializer().serializeToString(resultDoc).replace(/ xmlns=\"(.*?)\"/g, '');
    if(!isNaN(parseInt(indent as string)))
        indent = ' '.repeat(indent as number);
    if(indent != "  ")
        resultXml = resultXml.replace(/  /g, indent as string);
    return resultXml;
};

/**
 * Add the `onchange` functions to an array of string.
 * @param strings_to_bind An array of string names. Names must be written in`dash-case`
 * and represent actual input fields ids in the DOM. They also must corresponding input fields
 * for the string IDs for the stringtable. `string_id_types` are used by default.
 * @param obj A unit or a tech that the strings belong to.
 */
function bindStrings(strings_to_bind = string_id_types, obj: Unit|Tech) {
    strings_to_bind.forEach(string_id => {
        $(`#${string_id}`).on("change", () => { obj.setString(string_id); });
        $(`#${string_id}-id`).on("change", () => { obj.setString(string_id); });
    });
}
/**
 * A warning for a possible misuse of string IDs:
 * - 0 - OK
 * - 1 - This string is already occupied by other text. Replacing it may corrupt the game.
 * - 2 - This string ID is already in use. It is recommended to use new strings for modded content.
 * - 3 - You have already used this ID for a different string.
 * - 4 - It is recommended to use unique IDs even for identical strings.
 */
type StringWarning = 0 | 1 | 2 | 3 | 4;
interface RecordUpdateResult {
    from?: StringRecord,
    to?: StringRecord
}
/**
 * Add/update a string in the `new_strings_record`.
 * @param source Which field contains this string. For example, `unit-editor-name`.
 * @param new_id New string ID. 
 * Leave blank to simply remove the string.
 * @param new_value New string text. 
 * Leave blank to simply remove the string.
 * @param old_id Old string ID. Used to remove the string's old version. 
 * Leave blank to add the string for the first time.
 * @param old_value Old string text. Used to remove the string's old version. 
 * Leave blank to add the string for the first time.
 * @returns An object that contains string records with warnings to be updated 
 * (either fixed in `.from` or conflicting in `.to`).
 */
function updateStringRecord(
        source:string,
        options: {
            new_id:number,
            new_value:string,
            old_id?:number,
            old_value?:string
        } | {
            new_id?:number,
            new_value?:string,
            old_id:number,
            old_value:string
        }) : RecordUpdateResult {
    let update: RecordUpdateResult = {};
    if(options.old_id !== undefined && options.old_value !== undefined && (options.new_value !== options.old_value || options.new_id !== options.old_id)) {
        try {
            if(new_strings_record[options.old_id] !== undefined && 
               new_strings_record[options.old_id][options.old_value] !== undefined && 
               new_strings_record[options.old_id][options.old_value].has(source)) {
                update.from = new_strings_record[options.old_id];
                new_strings_record[options.old_id][options.old_value].delete(source);
                if(new_strings_record[options.old_id][options.old_value].size === 0) {
                    delete new_strings_record[options.old_id][options.old_value];
                    if(Object.keys(new_strings_record[options.old_id]).length === 0) {
                        delete new_strings_record[options.old_id];
                    }
                }
                updateWarnings(options.old_id, update.from);
            }
        } catch (error) {
            console.error(error);
        }
    }
    if(options.new_id !== undefined && options.new_value !== undefined) {
        if(new_strings_record[options.new_id] === undefined) {
            new_strings_record[options.new_id] = {};
            new_strings_record[options.new_id][options.new_value] = new Set([source]);
        } else {
            if(new_strings_record[options.new_id][options.new_value] === undefined)
                new_strings_record[options.new_id][options.new_value] = new Set([source]);
            else
                new_strings_record[options.new_id][options.new_value].add(source);
        }
        update.to = new_strings_record[options.new_id];
        updateWarnings(options.new_id, update.to);
    }
    console.log(update);
    return update;
}

function updateWarnings(id: number, records: StringRecord) {
    let conflicts: Record<string, StringWarning> = {};
    if(records !== undefined) {
        Object.keys(records).forEach(function (value) {
            [...records[value]].forEach(function (element) {
                conflicts[element] = 0;
                if (getString(id) != '') {
                    if (getString(id) != value)
                        conflicts[element] = 1;
                    else
                        conflicts[element] = 2;
                }
                else if(Object.keys(records).length > 1) {
                    conflicts[element] = 3;
                }
                else if(Object.keys(records).length == 1) {
                    if([...records[value]].length > 1)
                        conflicts[element] = 4;
                }
            });
        });
        Object.keys(conflicts).forEach(element => {
            $(`#${element}-warning`).removeClass("warning");
            $(`#${element}-warning`).removeClass("critical");
            switch (conflicts[element]) {
                case 0:
                    break;
                case 1:
                    $(`#${element}-warning`).addClass("critical");
                    $(`#${element}-warning .warning-tooltip`).text("This string is already occupied by other text. Replacing it may corrupt the game.");
                    break;
                case 2:
                    $(`#${element}-warning`).addClass("warning");
                    $(`#${element}-warning .warning-tooltip`).text("This string ID is already in use. It is recommended to use new strings for modded content.");
                    break;
                case 3:
                    $(`#${element}-warning`).addClass("critical");
                    $(`#${element}-warning .warning-tooltip`).text( "You have already used this ID for a different string.");
                    break;
                case 4:
                    $(`#${element}-warning`).addClass("warning");
                    $(`#${element}-warning .warning-tooltip`).text( "It is recommended to use unique IDs even for identical strings.");
                    break;
                default:
                    break;
            }
        });
        console.log(conflicts);
    }
}

/**
 * Finds the highest bonus multiplier of an attack against a unit (possibly less than 1.0).
 * @param attack An attack containing multipliers.
 * @param enemy_unit An adjustment unit to evaluate the attack against.
 * @returns Multiplier coefficient against the enemy; 1.0 if no specific multipliers were found.
 */
function getMultiplier(attack: Attack, enemy_unit: AdjustmentUnit) {
    let multiplier = -1;
    for (const category in attack.multipliers) {
        if (attack.multipliers.hasOwnProperty(category)) {
            const bonus = attack.multipliers[category];
            if(enemy_unit.categories.includes(category)) {
                //console.log(`Bonus against ${category}: ${bonus}`);
                multiplier = Math.max(multiplier, bonus);
            }
        }
    }
    if(multiplier === -1)
        multiplier = 1;
    //console.log(`Total multiplier against ${enemy_unit.name}: ${multiplier}`);
    return(multiplier);
}
/**
 * Get a string from the loaded stringtable by its ID.
 * @param locid String's ID in the stringtable.
 * @returns The string's text content.
 */
function getString(locid: number|string) {
    return $strings.find(`string[_locid=${locid}]`).text();
}
/**
 * Get a tag content or an attribute from an XML.
 * @param query A JQuery pattern to find, like `cost[resourcetype="Food"]`.
 * @param xml_query A JQuery XML to find the attribute/tag in.
 * @returns The attribute value/text content.
 */
function get(query: string, xml_query: JQuery<HTMLElement>) {
    if (xml_query.attr(query) !== undefined)
        return xml_query.attr(query);
    return xml_query.find(query).text();
}

/**
 * Converts a **`camelCaseString`** to **`snake_case_string`**.
 * @param str A camel-case string to convert.
 * @returns The converted snake-case string.
 */
function camelToSnakeCase(str: string): string {
    str = str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);
    return (str[0] === '_' ? str.slice(1) : str);
}
/**
 * Converts a **`dash-case-string`** to **`snake_case_string`**.
 * @param str A dash-case string to convert.
 * @returns The converted snake-case string.
 */
function dashToSnakeCase(str: string): string {
    return (str.replace(/-/g, "_"));
}
/**
 * Converts a **`camelCaseString`** to **`dash-case-string`**.
 * @param str A camel-case string to convert.
 * @returns The converted dash-case string.
 */
function camelToDashCase(str: string): string {
    str = str.replace(/[A-Z]/g, letter => `-${letter.toLowerCase()}`);
    return (str[0] === '-' ? str.slice(1) : str);
}
/**
 * Converts a **`dash-case-string`** to **`camelCaseString`**.
 * @param str A dash-case string to convert.
 * @returns The converted camel-case string.
 */
function dashToCamelCase(str: string): string {
    return (('-' + str).replace(/-([a-z])/g, g => g[1].toUpperCase()));
}


type AgeTech = "Colonialize"|"Fortressize"|"Industrialize"|"Imperialize";

type Resource = "food"|"wood"|"gold"|"trade"|"shipment";

type TechStringType = "display_name"|"rollover"|"new_name";

type Relativity = "BasePercent"|"Absolute"|"Percent"|"Assign";

/**
 * A class for unit upgrades. Binds the DOM's representation of an upgrade 
 * to the actual tech and its XML. Each tech is rendered idividually at 
 * runtime with TSX.
 */
class Tech {
    $xml: any;
    xml_editor: CodeMirror.Editor;
    element_name: string;
    name: string;
    display_name: string;
    rollover: string;
    name_template: string;
    display_name_template: string;
    rollover_template: string;
    new_name_template: string;
    display_name_id: number;
    rollover_id: number;
    id: number;
    icon: string;
    research_time: number;
    update_visual: boolean;
    new_name_id: number;
    new_name: string;
    age: "none"|AgeTech;
    prerequisites: string[];
    activated: string[];
    hpIncrease: number;
    hpRelativity: string;
    damageIncrease: number;
    damageRelativity: string;
    damageAction: string;
    food_cost: number;
    wood_cost: number;
    gold_cost: number;

    constructor(xml: HTMLElement) {
        this.$xml = $( xml );
        this.getName();
        this.getDisplayName();
        this.getRollover();
        this.getID();
        this.getIcon();
        this.getResearchTime();
        this.getCost();
        this.getUpdateVisual();
        this.new_name_id = null;
        this.new_name = '';
        this.getNewUnitName();
        this.getPrerequisites();
        this.getActivated();
        this.hpIncrease = 1.0;
        this.hpRelativity = "BasePercent";
        this.getHPIncrease();
        this.damageIncrease = 1.0;
        this.damageRelativity = "BasePercent";
        this.getDamageIncrease();
    }
    /**
     * Get a tech's DBID from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getID($xml: JQuery<HTMLElement> = $(this.$xml)) {
        this.id = parseInt($xml.find("dbid").text());
    }
    /**
     * Get a tech's name from its XML and generates the corresponding HTML element name in dash-case.
     * @param $xml The tech's JQuery XML element.
     */
    private getName($xml: JQuery<HTMLElement> = $(this.$xml)) {
        this.name = $xml.attr("name");
        this.element_name = camelToDashCase(this.name);
        this.name_template = this.name.replace(current_unit.name, '#T#');
    }
    /**
     * Get a tech's displayed name string and its ID from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getDisplayName($xml: JQuery<HTMLElement> = $(this.$xml)) {
        this.display_name_id = parseInt($xml.find(`displaynameid`).text());
        this.display_name = getString(this.display_name_id);
        this.display_name_template = this.display_name.replace(current_unit.display_name, '#T#');
    }
    /**
     * Get a tech's rollover string and its ID from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getRollover($xml: JQuery<HTMLElement> = $(this.$xml)) {
        this.rollover_id = parseInt($xml.find(`rollovertextid`).text());
        this.rollover = getString(this.rollover_id);
        this.rollover_template = this.rollover.replace(current_unit.display_name, '#T#');
    }
    /**
     * Get an upgrade's new name for a unit from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getNewUnitName($xml: JQuery<HTMLElement> = $(this.$xml)) {
        if ($xml.find(`effects>effect[type="SetName"]`).length > 0) {
            this.new_name_id = parseInt($xml.find(`effects>effect[type="SetName"]`).attr("newname"));
            this.new_name = getString(this.new_name_id);
            this.new_name_template = this.new_name.replace(current_unit.display_name, '#T#');
        }
    }
    /**
     * Get a tech's "update visuals" boolean from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getUpdateVisual($xml: JQuery<HTMLElement> = $(this.$xml)) {
        this.update_visual = ($xml.find(`effects>effect[subtype="UpdateVisual"]`).length > 0);
    }
    /**
     * Get a tech's research cost in all four resources from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getCost($xml: JQuery<HTMLElement> = $(this.$xml)) {
        ["Food", "Wood", "Gold", "Trade"].forEach(resource => {
            let cost = $xml.find(`cost[resourcetype="${resource}"]`).text();
            if (cost == '')
                cost = '0';
            this[`${resource.toLowerCase()}_cost` as `${Resource}_cost`] = parseInt(cost);
        });
    }
    /**
     * Get a tech's research time from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getResearchTime($xml: JQuery<HTMLElement> = $(this.$xml)) {
        this.research_time = parseInt($xml.find("researchpoints").text());
    }
    /**
     * Get a tech's icon from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getIcon($xml: JQuery<HTMLElement> = $(this.$xml)) {
        this.icon = $xml.find("icon").text();
    }
    /**
     * Get an upgrade's damage increase coefficient and the relativity mode from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getDamageIncrease($xml: JQuery<HTMLElement> = $(this.$xml)) {
        if ($xml.find(`effects>effect[subtype="Damage"]`).length > 0) {
            this.damageIncrease = parseFloat($xml.find(`effects>effect[subtype="Damage"]`).attr("amount"));
            this.damageRelativity = $xml.find(`effects>effect[subtype="Damage"]`).attr("relativity");
            if($xml.find(`effects>effect[subtype="Damage"]`).attr("allactions") === "1")
                this.damageAction = "all";
            else
                this.damageAction = $xml.find(`effects>effect[subtype="Damage"]`).attr("action");
        }
    }
    /**
     * Get an upgrade's HP increase coefficient and the relativity mode from its XML.
     * @param $xml The tech's JQuery XML element.
     */
    private getHPIncrease($xml: JQuery<HTMLElement> = $(this.$xml)) {
        if ($xml.find(`effects>effect[subtype="Hitpoints"]`).length > 0) {
            this.hpIncrease = parseFloat($xml.find(`effects>effect[subtype="Hitpoints"]`).attr("amount"));
            this.hpRelativity = $xml.find(`effects>effect[subtype="Hitpoints"]`).attr("relativity");
        }
    }
    /**
     * Run through the prerequisites and separate the age restrictions from actual prerequisite techs.
     * @param $xml The tech's JQuery XML element.
     */
    private getPrerequisites($xml:JQuery<HTMLElement> = $(this.$xml)) {
        this.age = "none";
        this.prerequisites = [];
        if($xml.find(`prereqs>techstatus[status="Active"]`).length > 0) {
            let age = $xml.find(`prereqs>techstatus[status="Active"]`);
            for (const prereq of age) {
                if(prereq.textContent.endsWith("ize"))
                    this.age = prereq.textContent as AgeTech;
                else
                    this.prerequisites.push(prereq.textContent);
            }
        }
    }
    /**
     * Run through the effects and get a list of techs that are being set to active.
     * @param $xml  The tech's JQuery XML element.
     */
    private getActivated($xml:JQuery<HTMLElement> = $(this.$xml)) {
        this.activated = [];
        for (const tech of $xml.find(`effects>effect[type="TechStatus"][status="active"]`))
            this.activated.push(tech.textContent);
    }

    /**
     * Generates the tech's HTML element to add into the DOM. Uses JSX.
     * @returns The rendered HTML for the tech.
     */
    render() {
        let tech_html:HTMLElement = 
        <div id={`${this.element_name}-tech`}>
            <table id={this.element_name} class="tech-container">
                <thead>
                    <tr>
                        <th colspan="4">
                            <h5>
                                <span class="warning-sign id-warning" id={`${this.element_name}-id-warning`}>
                                    <span class="tooltip-container persistent">⚠</span>
                                    <div class="warning-tooltip">
                                        <div>This DBID is already occupied by another technology.</div>
                                        <button id={`fix-${this.element_name}-dbid`} class="tech-dbid-fix-button">
                                            <svg viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M15.199 9.945a2.6 2.6 0 0 1-.79-1.551l-.403-3.083-2.73 1.486a2.6 2.6 0 0 1-1.72.273L6.5 6.5l.57 3.056a2.6 2.6 0 0 1-.273 1.72l-1.486 2.73 3.083.403a2.6 2.6 0 0 1 1.55.79l2.138 2.257 1.336-2.807a2.6 2.6 0 0 1 1.23-1.231l2.808-1.336-2.257-2.137zm.025 5.563l-2.213 4.65a.6.6 0 0 1-.977.155l-3.542-3.739a.6.6 0 0 0-.357-.182l-5.107-.668a.6.6 0 0 1-.449-.881l2.462-4.524a.6.6 0 0 0 .062-.396L4.16 4.86a.6.6 0 0 1 .7-.7l5.063.943a.6.6 0 0 0 .396-.062l4.524-2.462a.6.6 0 0 1 .881.45l.668 5.106a.6.6 0 0 0 .182.357l3.739 3.542a.6.6 0 0 1-.155.977l-4.65 2.213a.6.6 0 0 0-.284.284zm.797 1.927l1.414-1.414 4.243 4.242-1.415 1.415-4.242-4.243z"/></svg>
                                        </button>
                                    </div>
                                </span>
                                <span>
                                    <span class="comment">#</span>
                                    <input id={`${this.element_name}-id`} class="tech-id comment" type="number" value={this.id}/>
                                </span>
                                <span class="warning-sign name-warning" id={`${this.element_name}-name-warning`}>
                                    <span class="tooltip-container">⚠</span>
                                    <span class="warning-tooltip">This name is already used by another technology.</span>
                                </span>
                                <span>
                                    <input id={`${this.element_name}-name`} class="mid-text" value={this.name}/>
                                </span>
                            </h5>
                            <button id={`${this.element_name}-delete-button`} class="tech-delete-button">
                                <svg viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm0-9.414l2.828-2.829 1.415 1.415L13.414 12l2.829 2.828-1.415 1.415L12 13.414l-2.828 2.829-1.415-1.415L10.586 12 7.757 9.172l1.415-1.415L12 10.586z"/></svg>
                            </button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tech-fixed-width tag">Displayed name:</td>
                        <td class="warning-sign" id={`${this.element_name}-display-name-warning`}>
                            <span class="tooltip-container persistent">⚠</span>
                            <span class="warning-tooltip"></span>
                        </td>
                        <td>
                            <input type="number" class="numeric string-id" id={`${this.element_name}-display-name-id`} value={this.display_name_id}/>
                        </td>
                        <td>
                            <input id={`${this.element_name}-display-name`} class="short-text" value={this.display_name}/>
                        </td>
                    </tr>
                    <tr>
                        <td class="tech-fixed-width tag">Rollover:</td>
                        <td class="warning-sign" id={`${this.element_name}-rollover-warning`}>
                            <span class="tooltip-container persistent">⚠</span>
                            <span class="warning-tooltip"></span>
                        </td>
                        <td>
                            <input type="number" class="numeric string-id" id={`${this.element_name}-rollover-id`} value={this.rollover_id}/>
                        </td>
                        <td>
                            <input id={`${this.element_name}-rollover`} class="long-text" value={this.rollover}/>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table id={`${this.element_name}-cost`} class="tech-cost">
                <thead></thead>
                <tbody>
                    <tr>
                        <td id={`${this.element_name}-food-tag`} class="tag cost-tag">Food:</td>
                        <td id={`${this.element_name}-food`}><span class="food-icon">🥩 </span>
                            <input class="numeric cost" id={`${this.element_name}-food-cost`} type="number" step="50" min="0" value={this.food_cost}/>
                        </td>
                        <td>  </td>
                        <td id={`${this.element_name}-icon-tag`} class="tag">Icon:</td>
                        <td>
                            <input id={`${this.element_name}-icon`} class="mid-text" type="text" value={this.icon}/>
                        </td>
                    </tr>
                    <tr>
                        <td id={`${this.element_name}-wood-tag`} class="tag cost-tag">Wood:</td>
                        <td id={`${this.element_name}-wood`}><span class="wood-icon">🌳 </span>
                            <input class="numeric cost" id={`${this.element_name}-wood-cost`} type="number" step="50" min="0" value={this.wood_cost}/>
                        </td>
                        <td>  </td>
                        <td id={`${this.element_name}-research-time-tag`} class="tag">Research time:</td>
                        <td>
                            <span>🕔 </span><input class="numeric" id={`${this.element_name}-research-time`} type="number" min="0" value={this.research_time}/>
                        </td>
                    </tr>
                    <tr>
                        <td id={`${this.element_name}-gold-tag`} class="tag cost-tag">Gold:</td>
                        <td id={`${this.element_name}-gold`}><span class="gold-icon">💰 </span>
                            <input class="numeric cost" id={`${this.element_name}-gold-cost`} type="number" step="50" min="0" value={this.gold_cost}/>
                        </td>
                        <td>  </td>
                        <td id={`${this.element_name}-age-tag`} class="tag">Age:</td>
                        <td>
                            <select class="numeric" id={`${this.element_name}-age`} value={this.age /* No idea why this doesn't work here, so we change it manually later */}>
                                <option value="none">I</option>
                                <option value="Colonialize">II</option>
                                <option value="Fortressize">III</option>
                                <option value="Industrialize">IV</option>
                                <option value="Imperialize">V</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id={`${this.element_name}-prereqs-and-activated`} class="tech-prereqs-and-activated">
                <div id={`${this.element_name}-prereqs-tag`} class="tag">Prerequisites:</div>
                <div class="list">
                    <ol id={`${this.element_name}-prereqs`}></ol>
                    <button class="add add-prereqs"></button>
                </div>
                <div id={`${this.element_name}-activated-tag`} class="tag">Set active:</div>
                <div class="list">
                    <ol id={`${this.element_name}-activated`}></ol>
                    <button class="add add-activated"></button>
                </div>
            </div>
            <table id={`${this.element_name}-effects`} class="tech-effects">
                <thead></thead>
                <tbody>
                    <tr>
                        <td class="tech-fixed-width tag">Damage:</td>
                        <td>
                            <input id={`${this.element_name}-damage`} class="numeric" type="number" min="0.0" step="0.1" value={this.damageIncrease}/>
                        </td>
                        <td>
                            <span class="comment">relativity:</span>
                        </td>
                        <td>
                            <select name={`${this.element_name}-damage-relativity`} id={`${this.element_name}-damage-relativity`} value={this.damageRelativity}>
                                <option value="BasePercent">Base percent</option>
                                <option value="Absolute">Absolute</option>
                                <option value="Percent">Current percent</option>
                                <option value="Assign">Assign</option>
                            </select>
                        </td>
                        <td>
                            <span class="comment">actions:</span>
                        </td>
                        <td>
                            <select name={`${this.element_name}-damage-action`} id={`${this.element_name}-damage-action`}>
                                <option value="all">All</option>
                                <option value="VolleyHandAttack">Volley Hand Attack</option>
                                <option value="DefendHandAttack">Defend Hand Attack</option>
                                <option value="StaggerHandAttack">Stagger Hand Attack</option>
                                <option value="MeleeHandAttack">Melee Hand Attack</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="tech-fixed-width tag">HP:</td>
                        <td>
                            <input id={`${this.element_name}-hp`} class="numeric" type="number" min="0.0" step="0.1" value={this.hpIncrease}/>
                        </td>
                        <td>
                            <span class="comment">relativity:</span>
                        </td>
                        <td>
                            <select name={`${this.element_name}-hp-relativity`} id={`${this.element_name}-hp-relativity`} value={this.hpRelativity}>
                                <option value="BasePercent">Base percent</option>
                                <option value="Absolute">Absolute</option>
                                <option value="Percent">Current percent</option>
                                <option value="Assign">Assign</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td id={`${this.element_name}-new-name-tag`} class="tech-fixed-width tag">New name:</td>
                        <td style="min-width: 6rem;">
                            <span class="warning-sign" id={`${this.element_name}-new-name-warning`}>
                                <span class="tooltip-container persistent">⚠</span>
                                <span class="warning-tooltip"></span>
                            </span>
                            <span>
                                <input type="number" class="numeric string-id" id={`${this.element_name}-new-name-id`} value={this.new_name_id}/>
                            </span>
                        </td>
                        <td colspan="3">
                            <input id={`${this.element_name}-new-name`} class="short-text" value={this.new_name}/>
                        </td>
                    </tr>
                    <tr>
                        <td class="tech-fixed-width tag">Visuals:</td>
                        <td colspan="2">
                            <span style="display: flex; justify-content: center;">
                                <label id={`${this.element_name}-update-visual`} class="toggle" style="margin-top: 0.25rem;">
                                    <input type="checkbox" checked={this.update_visual}/>
                                    <span class="slider"></span>
                                </label>
                                <span class="comment">update</span>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id={`${this.element_name}-xml`} class="xml tech-xml" style={`display: ${(preferences["tech-xml"] && preferences["xml-editing"])? "block" : "none"};`}>
                <pre>
                    <textarea class="lang-xml"></textarea>
                </pre>
            </div>
        </div>
        this.prerequisites.forEach((prereq) => { tech_html = this.renderPrerequisite(prereq, $(tech_html))[0]; });
        this.activated.forEach((activated) => { tech_html = this.renderActivated(activated, $(tech_html))[0]; });
        $(tech_html).find(".add").html(` <svg viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M11 11V5h2v6h6v2h-6v6h-2v-6H5v-2z"/></svg> `);
        $(tech_html).find(".add-prereqs").on("click", () => {
            this.renderPrerequisite("");
            $(`#${this.element_name}-prereqs input`).last().trigger("focus");
        });
        $(tech_html).find(".add-activated").on("click", () => {
            this.renderActivated("");
            $(`#${this.element_name}-activated input`).last().trigger("focus");
        });
        return tech_html;
    }
    /**
     * Render an activated tech list item.
     * @param activated Activated tech name.
     * @param tech_html The HTML to add the tech into.
     */
    private renderPrerequisite(prereq: string, tech_html: JQuery<HTMLElement> = $(`#${this.element_name}-tech`)) {
        let elem = 
            <li>
                <input list="techs" class="tech-prerequisite" type="text" autocomplete="on" value={prereq} />
                <button id={`${this.element_name}-delete-button`} class="prereq-delete-button">
                    <svg viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z" /><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm0-9.414l2.828-2.829 1.415 1.415L13.414 12l2.829 2.828-1.415 1.415L12 13.414l-2.828 2.829-1.415-1.415L10.586 12 7.757 9.172l1.415-1.415L12 10.586z" /></svg>
                </button>
            </li>;
        $(elem).find("button")[0].innerHTML += ' ';
        $(elem).find("button").on("click", function () {
            this.parentElement.remove();
        });
        $(elem).find("button").on("click", () => {
            this.updatePrerequisites();
        });
        $(elem).find("input").on("blur", () => {this.updatePrerequisites()});
        tech_html.find(`#${this.element_name}-prereqs`).append(elem);
        return tech_html;
    }
    /**
     * Render a prerequisite tech list item.
     * @param activated Prerequisite tech name.
     * @param tech_html The HTML to add the tech into.
     */
    private renderActivated(activated: string, tech_html: JQuery<HTMLElement> = $(`#${this.element_name}-tech`)) {
        let elem =
            <li>
                <input list="techs" class="tech-activated" type="text" autocomplete="on" value={activated} />
                <button id={`${this.element_name}-delete-button`} class="activated-delete-button">
                    <svg viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z" /><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm0-9.414l2.828-2.829 1.415 1.415L13.414 12l2.829 2.828-1.415 1.415L12 13.414l-2.828 2.829-1.415-1.415L10.586 12 7.757 9.172l1.415-1.415L12 10.586z" /></svg>
                </button>
            </li>;
        $(elem).find("button")[0].innerHTML += ' ';
        $(elem).find("button").on("click", function () {
            this.parentElement.remove();
        });
        $(elem).find("button").on("click", () => {
            this.updateActivated();
        });
        $(elem).find("input").on("blur", () => {this.updateActivated()});
        tech_html.find(`#${this.element_name}-activated`).append(elem);
        return tech_html;
    }

    /**
     * Adds `onclick` and `onchange` type functions for the tech's input fields.
     */
    bindToDocument() {
        $(`#${this.element_name}-id`).on("change", () => {this.setTechID()});
        $(`#fix-${this.element_name}-dbid`).on("click", () => {
            $(`#${this.element_name}-id`).val(max_tech_dbid + 1);
            this.setTechID(max_tech_dbid + 1);
            max_tech_dbid++;
            console.log(`#fix-${this.element_name}-dbid`);
            
        });
        $(`#${this.element_name}-name`).on("change", () => {this.setName()});
        $(`#${this.element_name}-age`).on("change", () => {this.setAge()});
        $(`#${this.element_name}-research-time`).on("change", () => {this.setNumber("research-time", "instant")});
        bindStrings([`${this.element_name}-display-name`, `${this.element_name}-rollover`, `${this.element_name}-new-name`], this);
        string_id_types = string_id_types.concat([`${this.element_name}-display-name`, `${this.element_name}-rollover`, `${this.element_name}-new-name`]);
        this.setTechID();
        this.setName();
        [`${this.element_name}-display-name`, `${this.element_name}-rollover`, `${this.element_name}-new-name`].forEach((string_type) => {
            this.setString(string_type);
        });
        (["food", "wood", "gold"] as const).forEach(resource => {
            $(`#${this.element_name}-${resource}-cost`).on("change", () => {this.setCost(resource);} );
        });
        $(`#${this.element_name}-icon`).on("change", () => {this.setIcon()});
        $(`#${this.element_name}-damage`).on("change", () => {this.setDamage()});
        $(`#${this.element_name}-damage-relativity`).on("change", () => {this.setDamage()});
        $(`#${this.element_name}-damage-action`).on("change", () => {this.setDamage()});
        $(`#${this.element_name}-hp`).on("change", () => {this.setHP()});
        $(`#${this.element_name}-hp-relativity`).on("change", () => {this.setHP()});
        $(`#${this.element_name}-update-visual input`).on("change", () => {this.setUpdateVisual()});
        $(`#${this.element_name}-delete-button`).on("click", () => {
            $(`#${this.element_name}`).parent().remove();
            ["display-name", "rollover", "new-name"].forEach((s: TechStringType) => {
                string_id_types.splice(string_id_types.indexOf(`${this.element_name}-${s}`), 1);
                updateStringRecord(`${this.element_name}-${s}`, {old_id: this[dashToSnakeCase(`${s}_id`)], old_value: this[dashToSnakeCase(s)]});
            });
            delete current_unit.upgrades[this.element_name];
        });
        bindPersistentTips(`#${this.element_name}`);
    }

    /**
     * Create a new XML editor for the tech, unless there exists one already
     * or the user preferences have XML Editing off.
     * @param initial_value The text put into the editor. Uses the unit's XML by default.
     */
    createXMLEditor(initial_value:string = prettyPrintXML(this.$xml[0].outerHTML)) {
        if(!preferences["tech-xml"] || !preferences["xml-editing"])
            return;
        $(`#${this.element_name}-xml textarea`).html('');
        $(`#${this.element_name}-xml textarea`).text(initial_value);
        this.xml_editor = CodeMirror.fromTextArea($(`#${this.element_name}-xml textarea`)[0], {
            value: initial_value,
            //keyMap: 'sublime', // Bugged rn
            lineNumbers: true,
            mode: "text/xml",
            indentWithTabs: false,
            readOnly: !preferences["live-xml"]
        });
    }
    /**
     * Replace the text in the XML editor with the one provided.
     * @param value The new text to be put into the editor. Uses the tech's XML by default.
     */
    updateXMLEditor(value:string = prettyPrintXML(this.$xml[0].outerHTML)) {
        if(this.xml_editor != undefined && preferences["tech-xml"] && preferences["xml-editing"])
            this.xml_editor.setValue(value.replace(/ xmlns=\"(.*?)\"/g, ''));
    }
    /**
     * Add, update or remove an XML tag.
     * @param tag_name Tag name to be added/changed/removed.
     * @param value The new value for the tag. If the tag value is numeric, but NaN, the tag will be removed.
     * @param after_tag An existing tag that the new one will follow if it doesn't exist yet.
     * @param is_string Is the tag value a string, i. e. is a NaN allowed as the value.
     * If not, and the value is `''` or other NaN, the tag will be removed. False by default.
     * @param xml The JQuery XML element to put the tag into. Uses the unit's XML by default.
     */
    private setXMLValue(tag_name: string, value: string|number, after_tag: string, is_string=false, xml=this.$xml) {
        if((!is_string && (isNaN(value as number) || value == 0)) || 
            (is_string && value == '')) {
            xml.find(tag_name).remove();
        }
        else {
            if(xml.find(tag_name).length === 0) {
                let build_limit = xmlDoc.createElement(tag_name);
                build_limit.textContent = value.toString();
                xml.find(after_tag).after(build_limit);
            }
            else
                xml.find(tag_name)[0].textContent = value.toString();
        }
    }

    /**
     * Change a tech's unique ID to the one specified. Also checks if the provided ID is unique.
     * @param new_id The new tech ID. Takes one from the `#${this.element_name}-id` input field by default.
     */
    setTechID(new_id = $(`#${this.element_name}-id`).val()) {
        this.id = parseInt(new_id as string);
        if ($techs.find(`tech>dbid:contains(${this.id})`).length > 0)
            $(`#${this.element_name}-id-warning`).addClass("critical");
        else
            $(`#${this.element_name}-id-warning`).removeClass("critical");
        this.$xml.find("dbid").text(this.id);
        this.updateXMLEditor();
    }
    /**
     * Change a tech's unique name to the one specified. Also checks if the provided name is unique.
     * If a naming template fits, the name will be generated from the unit's name.
     * @param new_name The new tech name. Takes one from the `#${this.element_name}-name` input field by default.
     * @param old_unit_name Old unit name to replace in a template. Takes the `current_unit.name`
     * (and hence doesn't do anything) if no value is provided.
     */
    setName(new_name:string = $(`#${this.element_name}-name`).val() as string, old_unit_name:string = current_unit.name) {
        const input_name = $(`#${this.element_name}-name`).val();
        const old_name = this.name;
        if(this.name_template.replace('#T#', old_unit_name) == input_name) {
            new_name = this.name_template.replace('#T#', current_unit.name);
            $(`#${this.element_name}-name`).val(new_name);
        }
        this.name = new_name;
        if ($techs.find(`tech[name="${this.name}"]`).length > 0)
            $(`#${this.element_name}-name-warning`).addClass("critical");
        else
            $(`#${this.element_name}-name-warning`).removeClass("critical");
        this.$xml.attr("name", this.name);
        for (const u in current_unit.upgrades) {
            if (Object.prototype.hasOwnProperty.call(current_unit.upgrades, u)) {
                const upgrade = current_unit.upgrades[u];
                $(`#${upgrade.element_name}-prereqs input`).map(function() {
                    console.log((this as HTMLInputElement).value);
                    console.log(old_name);
                    if((this as HTMLInputElement).value === old_name)
                        (this as HTMLInputElement).value = new_name;
                });
                upgrade.updatePrerequisites();
                $(`#${upgrade.element_name}-activated input`).map(function() {
                    console.log((this as HTMLInputElement).value);
                    console.log(old_name);
                    if((this as HTMLInputElement).value === old_name)
                        (this as HTMLInputElement).value = new_name;
                });
                upgrade.updateActivated();
            }
        }
        this.updateXMLEditor();
    }
    /**
     * Change a tech's allowed age to the one specified.
     * @param new_age New age for the tech. Takes one from the `#${this.element_name}-age` input field by default.
     */
    setAge(new_age:"none"|AgeTech = $(`#${this.element_name}-age`).val() as "none"|AgeTech) {
        this.age = new_age;
        this.setPrerequisites();
    }
    /**
     * Update the tech's XML with a list of prerequisite techs, including the age one.
     * @param prerequisites New prerequisites list. Takes `this.prerequisites` by default.
     */
    setPrerequisites(prerequisites: string[] = this.prerequisites) {
        this.prerequisites = prerequisites;
        this.$xml.find("prereqs").remove();
        this.$xml.find("effects").before(xmlDoc.createElement("prereqs"));
        if(this.age != "none") {
            let elem = <techstatus status="Active">{this.age}</techstatus>;
            this.$xml.find("prereqs").append(elem);
        }
        prerequisites.forEach(tech => {
            let elem = <techstatus status="Active">{tech}</techstatus>;
            this.$xml.find("prereqs").append(elem);
        });
        this.updateXMLEditor();
    }
    /**
     * Update the tech's XML with a list of activated techs.
     * @param activated New activated techs list. Takes `this.activated` by default.
     */
    setActivated(activated: string[] = this.activated) {
        this.activated = activated;
        this.$xml.find(`effects effect[type="TechStatus"][status="active"]`).remove();
        activated.forEach(tech => {
            let elem = <effect type="TechStatus" status="active">{tech}</effect>;
            this.$xml.find("effects").append(elem);
        });
        this.updateXMLEditor();
    }
    /**
     * Run through the prereqs specified in the DOM and set them.
     */
    async updatePrerequisites() {
        let new_prereqs = [];
        $(`#${this.element_name}-prereqs input`).map(function () {
            if ((this as HTMLInputElement).value !== '')
                new_prereqs.push((this as HTMLInputElement).value);

            else
                $(this.parentElement).find(".prereq-delete-button").trigger("click");
        });
        this.setPrerequisites(new_prereqs);
    }
    /**
     * Run through the activated techs specified in the DOM and set them.
     */
    async updateActivated() {
        let new_activated = [];
        $(`#${this.element_name}-activated input`).map(function () {
            if ((this as HTMLInputElement).value !== '')
                new_activated.push((this as HTMLInputElement).value);

            else
                $(this.parentElement).find(".activated-delete-button").trigger("click");
        });
        this.setActivated(new_activated);
    }
    /**
     * Change a tech's cost in a specific resource.
     * @param resource The resource to change.
     * @param new_cost New cost in specified resource. Takes one from the `#${this.element_name}-${resource}-cost` input field by default.
     */
    setCost(resource: Resource, new_cost: number = parseInt($(`#${this.element_name}-${resource}-cost`).val() as string)) {
        this[`${resource}_cost` as `${Resource}_cost`] = new_cost;
        if (this[`${resource}_cost` as `${Resource}_cost`] > 0) {
            $(`#${this.element_name}-${resource.toLowerCase()}-tag`).css("opacity", 0.9);
            $(`#${this.element_name}-${resource.toLowerCase()}`).css("opacity", 0.9);
        }
        else {
            $(`#${this.element_name}-${resource.toLowerCase()}-tag`).css("opacity", 0.5);
            $(`#${this.element_name}-${resource.toLowerCase()}`).css("opacity", 0.5);
        }
        let resource_upper = resource.slice(0, 1).toUpperCase() + resource.slice(1);
        if(this[`${resource}_cost` as `${Resource}_cost`] > 0 && this.$xml.find(`cost[resourcetype="${resource_upper}"]`).length === 0)
            this.$xml.find("displaynameid").after(<cost resourcetype={resource_upper}>{this[`${resource}_cost` as `${Resource}_cost`]}</cost>);
        else
            this.setXMLValue(`cost[resourcetype="${resource_upper}"]`, this[`${resource}_cost` as `${Resource}_cost`], "buildbounty");
        this.updateXMLEditor();
    }
    /**
     * Change the icon path. Update the XML accordingly.
     * @param new_icon New icon path. Takes one from DOM by default.
     */
    setIcon(new_icon: string = $(`#${this.element_name}-icon`).val() as string) {
        this.icon = new_icon;
        if (new_icon == '') {
            $(`#${this.element_name}-icon`).css("opacity", 0.5);
            $(`#${this.element_name}-icon-tag`).css("opacity", 0.5);
        }
        else {
            $(`#${this.element_name}-icon`).css("opacity", 1.0);
            $(`#${this.element_name}-icon-tag`).css("opacity", 1.0);
        }
        this.setXMLValue("icon", this.icon, "researchpoints", true);
        this.updateXMLEditor();
    }
    /**
     * Change a numeric tech property, such as `research_time`, to the one found in the corresponding DOM's input field.
     * @param property_name Which property of the tech's object is to be changed. **Should be written in`dash-case`**.
     * @param null_value What should zero/NaN value be replaced with, for example, `"inf"` or `1`. Default value is `0`.
     * @param is_float Are float values allowed. False by default.
     */
    setNumber(property_name: "research-time"|"hp-increase"|"damage-increase", null_value:number|string = 0, is_float=false) {
        const property_name_snake_case = dashToSnakeCase(property_name) as "research_time"|"hp_increase"|"damage_increase";
        const element_name = this.element_name + '-' + property_name;
        if (is_float)
            this[property_name_snake_case] = parseFloat($(`#${element_name}`).val() as string);
        else
            this[property_name_snake_case] = parseInt($(`#${element_name}`).val() as string);
        if (isNaN(this[property_name_snake_case]) || this[property_name_snake_case] == 0) {
            this[property_name_snake_case] = 0;
            $(`#${element_name}`).css("opacity", 0.5);
            $(`#${element_name}-tag`).css("opacity", 0.5);
            if (typeof(null_value) == "string")
                ($(`#${element_name}`)[0] as HTMLInputElement).type = "text";
                $(`#${element_name}`).val(null_value.toString());
        }
        else {
            $(`#${element_name}`).css("opacity", 0.9);
            $(`#${element_name}-tag`).css("opacity", 0.9);
            ($(`#${element_name}`)[0] as HTMLInputElement).type = "number";
        }
        if(property_name === "research-time")
            this.setXMLValue("researchpoints", this.research_time, "displaynameid");
        this.updateXMLEditor();
    }
    /**
     * Change a string and its stringtable ID in a tech.
     * @param string_name The string property of the tech's object to change, like `imp-musketeer-display-name`. 
     * **Should be written in`dash-case` and include tech name.**
     * @param new_string A new string value. Takes one from the DOM by default.
     */
    setString(string_name: string, new_string: string = undefined) {
        const string_type = dashToSnakeCase(string_name.replace(this.element_name + '-', '')) as TechStringType;
        const id_type = string_type + "_id" as `${TechStringType}_id`;
        const old_string = this[string_type];
        const old_id = this[id_type];
        const new_id = parseInt($(`#${string_name}-id`).val() as string);
        if(new_string === undefined)
            new_string = $(`#${string_name}`).val() as string;
        else
            $(`#${string_name}`).val(new_string);
        this[string_type] = new_string;
        this[id_type] = new_id;
    
        updateStringRecord(string_name, {new_id: new_id, new_value: new_string, old_id: old_id, old_value: old_string});

        if (string_name.endsWith("new-name") && (this[string_type] === '' || isNaN(new_id))) {
            $(`#${string_name}-id`).css("opacity", 0.5);
            $(`#${string_name}-tag`).css("opacity", 0.5);
            $(`#${string_name}`).css("opacity", 0.5);
        } else if (string_name.endsWith("new-name")) {
            $(`#${string_name}-id`).css("opacity", 1.0);
            $(`#${string_name}-tag`).css("opacity", 1.0);
            $(`#${string_name}`).css("opacity", 1.0);
        }
        switch (string_type) {
            case "display_name":
                this.$xml.find("displaynameid").text(new_id);
                break;
            case "rollover":
                this.setXMLValue("rollovertextid", new_id, "icon");
                break;
            case "new_name":
                this.$xml.find(`effect[type="SetName"]`).remove();
                if(new_string !== '' && !isNaN(new_id))
                    this.$xml.find("effects").append(<effect type="SetName" proto={current_unit.name} culture="none" newname={this.new_name_id}/>);
                break;
            default:
                break;
        }
        this.updateXMLEditor();
    }
    /**
     * Set new damage increase effect.
     * @param increase Increase value.
     * @param relativity Relativity, one of the `"BasePercent" | "Absolute" | "Percent" | "Assign"`.
     * @param action Whether to increase damage for `all` actions or a specific one.
     */
    setDamage(
            increase: number = parseFloat($(`#${this.element_name}-damage`).val() as string),
            relativity: Relativity = $(`#${this.element_name}-damage-relativity`).val() as Relativity,
            action: string = $(`#${this.element_name}-damage-action`).val() as string) {
        this.damageIncrease = increase;
        this.damageRelativity = relativity;
        this.damageAction = action;
        this.$xml.find(`effect[subtype="Damage"]`).remove();
        if(! (
            relativity == "Absolute" && increase == 0  ||  
            relativity == "BasePercent" && increase == 1  ||  
            relativity == "Percent" && increase == 1)) {
                if(action === "all")
                    this.$xml.find("effects").append(
                        <effect type="Data" amount={increase} subtype="Damage" allactions="1" relativity={relativity}>
                            <target type="ProtoUnit">{current_unit.name}</target>
                        </effect>
                    );
                else
                    this.$xml.find("effects").append(
                        <effect type="Data" action={action} amount={increase} subtype="Damage" relativity={relativity}>
                            <target type="ProtoUnit">{current_unit.name}</target>
                        </effect>
                    );
            }
        this.updateXMLEditor();
    }
    /**
     * Set new HP increase effect.
     * @param increase Increase value.
     * @param relativity Relativity, one of the `"BasePercent" | "Absolute" | "Percent" | "Assign"`.
     */
    setHP(
        increase: number = parseFloat($(`#${this.element_name}-hp`).val() as string),
        relativity: Relativity = $(`#${this.element_name}-hp-relativity`).val() as Relativity) {
        this.damageIncrease = increase;
        this.damageRelativity = relativity;
        this.$xml.find(`effect[subtype="Hitpoints"]`).remove();
        if(! (
            relativity == "Absolute" && increase == 0  ||  
            relativity == "BasePercent" && increase == 1  ||  
            relativity == "Percent" && increase == 1)) {
                this.$xml.find("effects").append(
                    <effect type="Data" amount={increase} subtype="Hitpoints" relativity={relativity}>
                        <target type="ProtoUnit">{current_unit.name}</target>
                    </effect>
                );
            }
        this.updateXMLEditor();
    }
    /**
     * Update whether to update visuals of the unit.
     * @param visual Whether to update visuals. Takes one from the DOM by default.
     */
    setUpdateVisual(visual: boolean = $(`#${this.element_name}-update-visual input`).prop("checked")) {
        this.update_visual = visual;
        this.$xml.find(`effect[subtype="UpdateVisual"][unittype="${current_unit.name}"]`).remove();
        if(this.update_visual)
            this.$xml.find("effects").append(<effect type="Data" amount="0.00" subtype="UpdateVisual" unittype={current_unit.name} relativity="Absolute"><target type="Player"/></effect>)
        this.updateXMLEditor();
    }
    /**
     * Update a string using the template after a unit is renamed.
     * @param string_name The string property of the tech's object to change, like `imp-musketeer-display-name`. 
     * **Should be written in`dash-case` and include tech name.**
     * @param old_string Old unit name to replace. Used to decide if the template is still relevant.
     */
    async updateTemplate(string_name: string, old_string: string) {
        const string_type = dashToSnakeCase(string_name.replace(this.element_name + '-', '')) as TechStringType;
        if(this[string_type] === this[string_type + "_template" as `${TechStringType}_template`].replace('#T#', old_string))
            this.setString(string_name, this[string_type + "_template" as `${TechStringType}_template`].replace('#T#', current_unit.display_name));
    }
}


type Multipliers = Record<string, number>;

interface Attack {
    damage: number,
    rof: number,
    range: number,
    damagearea: number,
    damagecap: number,
    multipliers: Multipliers
}

interface Protoaction {
    name: string,
    multipliers: Multipliers,
    xml: HTMLElement,
    damagearea: number,
    damagecap: number,
    damage: number,
    rof: number,
    range: number
}

type ArmorType = "Ranged"|"Hand"|"Siege";

type AttackType = "ranged"|"melee"|"siege"|"cannon";

type UnitStringType = "display_name"|"editor_name"|"rollover"|"shortrollover";

const action_types = {
    "HandAttack": "melee",
    "GuardianAttack": "melee",
    "ChargeAttack": "melee",
    "RangedAttack": "ranged",
    "VolleyAttack": "ranged",
    "BowAttack": "ranged",
    "GrenadeAttack": "ranged",
    "BuildingAttack": "siege",
    "SiegeAttack": "siege",
    "CannonAttack": "cannon",
    "Attack": "attacks",
    "": "other"
};

interface Building {
    name: string;
    is_main: boolean;
    column: number;
};

class Unit {
    $xml: JQuery<any>;
    id: number;
    name: string;
    display_name_id: number;
    display_name: string;
    editor_name_id: number;
    editor_name: string;
    editor_name_template: string;
    rollover_id: number;
    rollover: string;
    shortrollover_id: number;
    shortrollover: string;
    file_path: string;
    file_path_template: string;
    total_cost: number;
    population: number;
    train_time: number;
    build_limit: number;
    age: number;
    hp: number;
    armor: number;
    armor_type: any;
    speed: number;
    los: number;
    adjusted_hp: number;
    protoactions: Record<AttackType|"attacks"|"other", Protoaction[]>
    main_attacks: Record<AttackType, Attack>;
    base_attacks: any;
    adjusted_attack: number;
    specialization: number;
    efficiency: number;
    unit_xml_editor: CodeMirror.Editor;
    upgrades: Record<string, Tech>;
    buildings: Building[];
    food_cost: number;
    wood_cost: number;
    gold_cost: number;
    trade_cost: number;

    constructor(xml: JQuery<HTMLElement>) {
        this.$xml = xml;
        this.getUnitID();
        this.getName();
        this.getDisplayName();
        this.getRollover();
        this.getFilePath();
        this.getCost();
        this.getPopulation();
        this.train_time = parseInt(get("trainpoints", xml));
        this.build_limit = parseInt(get("buildlimit", xml));
        this.age = parseInt(get("allowedage", xml));
        this.hp = parseInt(get("maxhitpoints", xml));
        this.getArmor();
        this.speed = parseFloat(get("maxvelocity", xml));
        this.los = parseFloat(get("los", xml));

        this.getMainAttacks(xml);
        this.getAttack(this.main_attacks, false, false);
        this.specialization = 0;
        this.efficiency = 0;
        setTimeout(() => {
            Promise.all([
                this.getUpgrades(), 
                this.getBuildings()
            ]).then(() => {
                $(".tech-delete-button").map(function() { this.innerHTML += ' ' });
                $(".tech-dbid-fix-button").map(function() { this.innerHTML += ' Fix' });
                this.getMainBuildings().then(() => {
                this.renderAllBuildings(); });
            });
        }, 100);
    }
    /**
     * Get the unit's ID.
     */
    private getUnitID() {
        this.id = parseInt(get("id", this.$xml));
    }
    /**
     * Get the unit's name.
     */
    private getName() {
        this.name = get("name", this.$xml);
    }
    /**
     * Get the unit's ingame displayed name and the editor name strings (same as the 
     * displayed name if none found), as well as their IDs.
     */
    private getDisplayName() {
        this.display_name_id = parseInt(get("displaynameid", this.$xml));
        this.display_name = getString(get("displaynameid", this.$xml));
        try {
            this.editor_name_id = parseInt(get("editornameid", this.$xml));
            this.editor_name = getString(get("editornameid", this.$xml));
        } catch {
            this.editor_name_id = this.display_name_id;
            this.editor_name = this.display_name;
        }
        this.editor_name_template = this.editor_name.replace(RegExp(this.display_name, 'g'), '#T#');
    }
    /**
     * Get the unit's rollover and shortrollover strings and their IDs.
     */
    private getRollover() {
        this.rollover_id = parseInt(get("rollovertextid", this.$xml));
        this.rollover = getString(get("rollovertextid", this.$xml));
        this.shortrollover_id = parseInt(get("shortrollovertextid", this.$xml));
        this.shortrollover = getString(get("shortrollovertextid", this.$xml));
    }
    /**
     * Generate the unit's directory in the `art` folder for its icons, models, textures and anim XMLs.
     * Tries to get a template for the path, like `units\infantry_ranged\`**`#T#`**`\`.
     */
    private getFilePath() {
        this.file_path = get("animfile", this.$xml);
        let unit_folder = this.getTrimmedName();
        if (this.file_path.includes('.'))
            this.file_path = this.file_path.split('\\').slice(0, -1).join('\\') + '\\';
        this.file_path_template = this.file_path.replace(RegExp(unit_folder, 'g'), '#T#');
        if(! this.file_path_template.includes('#T#'))
            this.file_path_template = this.file_path.slice(0, this.file_path.indexOf(unit_folder)) + '\\#T#\\';
    }
    /**
     * Trim the `yp`, `Nat`, etc. from the unit name and convert it to snake-case.
     * @param name Original unit name; `this.name` by default.
     * @returns Trimmed snake-case name
     */
    private getTrimmedName(name = this.name) {
        ["xp", "yp", "Nat", "Merc"].forEach((prefix) => {
            if (name.startsWith(prefix))
                name = name.slice(prefix.length);
        });
        return camelToSnakeCase(name);
    }
    /**
     * Get the unit's cost in all four resources and sum up the total cost.
     */
    private getCost() {
        this.total_cost = 0;
        ["Food", "Wood", "Gold", "Trade"].forEach(resource => {
            let cost = get(`cost[resourcetype=${resource}]`, this.$xml);
            if (cost == '')
                cost = '0';
            this[`${resource.toLowerCase()}_cost` as `${Resource}_cost`] = parseInt(cost);
            this.total_cost += parseInt(cost);
        });
    }
    /**
     * Get the unit's population; 0 if none found.
     */
    private getPopulation() {
        this.population = parseInt(get("populationcount", this.$xml));
        if (isNaN(this.population))
            this.population = 0;
    }
    /**
     * Get the unit's HP, adjusted for the armor.
     * @param hp The unit's hp. Takes the current by default.
     * @param armor The unit's armor. Takes the current by default.
     */
    private getAdjustedHP(hp:number = this.hp, armor:number = this.armor) {
        this.adjusted_hp = Math.floor(hp / (1 - armor / 2.5));
    }
    /**
     * Get the unit's armor type and value. Update adjusted HP unless told otherwise.
     * @param update_adjusted_hp Whether the unit's adjusted HP should be recalculated. True by default.
     */
    private getArmor(update_adjusted_hp = true) {
        this.armor = parseFloat(this.$xml.find("armor").attr("value"));
        this.armor_type = this.$xml.find("armor").attr("type");
        if (update_adjusted_hp)
            this.getAdjustedHP();
    }
    /**
     * Get four main attack types from a unit's protoactions.
     * @param $xml The unit's JQuery XML element.
     */
    private getMainAttacks($xml: JQuery<HTMLElement>) {
        const actions = $xml.find("protoaction");
        const action_types: Record<string, AttackType|"attacks"|"other"> = {
            "HandAttack" : "melee",
            "ChargeAttack" : "melee",
            "RangedAttack" : "ranged",
            "VolleyAttack" : "ranged",
            "BowAttack" : "ranged",
            "GrenadeAttack" : "ranged",
            "BuildingAttack" : "siege",
            "CannonAttack" : "cannon",
            "Attack" : "attacks",
            "": "other"
        }
        this.protoactions = {
            melee: [],
            ranged: [],
            siege: [],
            cannon: [],
            attacks: [],
            other: []
        };
        this.main_attacks = {
            melee: { damage: 0, rof: 3, range: 0, damagearea: 0, damagecap: 0, multipliers: {} },
            ranged: { damage: 0, rof: 3, range: 0, damagearea: 0, damagecap: 0, multipliers: {} },
            siege: { damage: 0, rof: 3, range: 0, damagearea: 0, damagecap: 0, multipliers: {} },
            cannon: { damage: 0, rof: 3, range: 0, damagearea: 0, damagecap: 0, multipliers: {} }
        };
        for (const key in actions) {
            if (actions.hasOwnProperty(key) && typeof(actions[key]) == "object") {
                const action = actions[key];
                try {
                    let protoaction: Protoaction = {
                        name: action.getElementsByTagName("name")[0].textContent,
                        multipliers: {},
                        xml: action,
                        damagearea: 0,
                        damagecap: 0,
                        damage: 0,
                        rof: 3.0,
                        range: 0
                    };
                    let action_type: AttackType|"attacks"|"other";
                    for (const attack_type in action_types)
                        if (action_types.hasOwnProperty(attack_type) && action.getElementsByTagName("name")[0].textContent.endsWith(attack_type)) {
                            action_type = action_types[attack_type];
                            break;
                        }
                    protoaction["damage"] = parseFloat(action.getElementsByTagName("damage")[0].textContent);
                    protoaction["rof"] = parseFloat(action.getElementsByTagName("rof")[0].textContent);
                    if (action.getElementsByTagName("maxrange").length)
                        protoaction["range"] = parseFloat(action.getElementsByTagName("maxrange")[0].textContent);
                    if (action.getElementsByTagName("damagearea").length) {
                        protoaction["damagearea"] = parseFloat(action.getElementsByTagName("damagearea")[0].textContent);
                        protoaction["damagecap"] = parseFloat(action.getElementsByTagName("damagecap")[0].textContent);
                    }
                    let bonuses = action.getElementsByTagName("damagebonus");
                    for (const _ in bonuses)
                        if (bonuses.hasOwnProperty(_))
                            protoaction["multipliers"][bonuses[_].getAttribute("type")] = parseFloat(bonuses[_].textContent);
                    this.protoactions[action_type].push(protoaction);
                } catch(e) { console.log(e); }
            }
        }
    }
    /**
     * Get the unit's attacks, their multipliers and area damage from four major attacks
     * previously extracted from protoaction. Update adjusted attack and cost efficiency 
     * unless told otherwise.
     * @param main_attacks The unit's four major attack types.
     * @param update_adjusted_attack  Should the unit's adjusted attack be recalculated. True by default.
     * @param update_efficiency  Should the unit's cost efficiency be recalculated. True by default.
     * @returns The highest attack weighted only for ROF.
     */
    private getAttack(main_attacks = this.main_attacks, update_adjusted_attack = true, update_efficiency = true) {
        for (const type in main_attacks) {
            if (main_attacks.hasOwnProperty(type as AttackType)) {
                this.protoactions[type as AttackType].forEach(action => {
                    if (action.damage / action.rof > main_attacks[type as AttackType].damage / main_attacks[type as AttackType].rof) {
                        main_attacks[type as AttackType].damage = action.damage;
                        main_attacks[type as AttackType].rof = action.rof;
                        main_attacks[type as AttackType].range = action.range;
                        main_attacks[type as AttackType].multipliers = action.multipliers;
                        main_attacks[type as AttackType].damagearea = action.damagearea;
                        main_attacks[type as AttackType].damagecap = Math.max(action.damagecap, action.damage);
                    }
                });
            }
        }
        this.base_attacks = main_attacks;
        let attack = Math.max(main_attacks.ranged.damage / main_attacks.ranged.rof, main_attacks.melee.damage / main_attacks.melee.rof);
        if (attack == 0)
            attack = Math.max(main_attacks.cannon.damage / main_attacks.cannon.rof, main_attacks.siege.damage / main_attacks.siege.rof);
        this.adjusted_attack = Math.floor(12 * attack) / 4;
        if (update_adjusted_attack)
            this.getAdjustedAttack(adjustment_units, update_efficiency);
        return 3 * attack;
    }
    /**
     * Calculate the attack adjusted for a list of possible enemies and some other parameters. Then update efficiency, unless told otherwise.
     * @param enemies An array of enemies to test against. Uses `adjustment_units` list by default.
     * @param update_efficiency Should the unit's cost efficiency be recalculated. True by default.
     * @returns The average attack weighted for area damage, 3.0 ROF and ranged/melee combat.
     */
    private getAdjustedAttack(enemies:AdjustmentUnit[] = adjustment_units, update_efficiency = true) {
        let adjusted_attack = 0;
        let weight = 0;
        let specialization = 0;
        //console.log(`____________________________________\nAdjusted attacks:`)
        enemies.forEach(enemy => {
            let current_adjusted_attack = 0;
            let current_specialization = 0;
            let current_weight = 0;
            for (const attack_type in this.main_attacks) {
                if (this.main_attacks.hasOwnProperty(attack_type) && this.main_attacks[attack_type as AttackType].damage > 0) {
                    const attack = this.main_attacks[attack_type as AttackType];
                    let damage = attack.damage;
                    if(attack.damagecap > 0) {
                        damage = (damage + attack.damagecap) / 2
                    }
                    switch (enemy.type) {
                        case "ranged":
                            switch (attack_type) {
                                case "melee":
                                    current_adjusted_attack += 0.2 * (0.75 * getMultiplier(attack, enemy) + 0.25) * damage / attack.rof;
                                    current_specialization += 0.2 * (getMultiplier(attack, enemy) - 1) ** 2;
                                    current_weight += 0.3;
                                    break;
                                case "ranged":
                                case "cannon":
                                    current_adjusted_attack += 0.8 * (0.75 * getMultiplier(attack, enemy) + 0.25) * damage / attack.rof;
                                    current_specialization += 0.8 * (getMultiplier(attack, enemy) - 1) ** 2;
                                    current_weight += 0.7;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "melee":
                            switch (attack_type) {
                                case "ranged":
                                case "melee":
                                case "cannon":
                                    current_adjusted_attack += (0.75 * getMultiplier(attack, enemy) + 0.25) * damage / attack.rof;
                                    current_specialization += (getMultiplier(attack, enemy) - 1) ** 2;
                                    current_weight += 1;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "building":
                            switch (attack_type) {
                                case "siege":
                                case "cannon":
                                    current_adjusted_attack += (0.75 * getMultiplier(attack, enemy) + 0.25) * damage / attack.rof;
                                    current_specialization += (getMultiplier(attack, enemy) - 1) ** 2;
                                    current_weight += 1;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            if(current_weight > 0) {
                adjusted_attack += current_adjusted_attack * enemy.weight / current_weight;
                specialization += current_specialization * enemy.weight / current_weight;
                weight += enemy.weight;
                //console.log(enemy.name);
                //console.log(Math.round(current_adjusted_attack * 3 / current_weight));
                //console.log(`Specialization: ${current_specialization / current_weight}`);
            }
        });
        this.adjusted_attack = adjusted_attack * 3 / weight;
        adjusted_attack = Math.round(this.adjusted_attack * 2) / 2;
        this.specialization = Math.min(100, Math.round(10 * Math.sqrt(specialization / weight)));
        $("#unit-main-attack").text(adjusted_attack.toString());
        $("#unit-specialization").text(this.specialization + '%');
        if(update_efficiency)
            this.getEfficiency();
        return(Math.round(adjusted_attack));
    }
    /**
     * Calculate the approximate cost efficiency of a unit.
     */
    private getEfficiency() {
        this.efficiency = Math.round(450 * (
                (Math.sqrt(
                    this.adjusted_attack * (Math.max(10, this.adjusted_hp - 100)) * (1 + this.specialization)
                ) + 30 * (this.speed - 3) + 5 * (this.los - 10)) / (this.total_cost ** 2)
            ) ** 0.3 ) / 100;
        $("#unit-efficiency").text(this.efficiency.toString());
    }

    /**
     * Fills in the unit's input fields in the DOM. Also creates the XML editor and binds onChange actions.s
     */
    render() {
        $("#unit-main-name").text(this.display_name);
        $("#unit-id").val(this.id.toString());
        this.setUnitID();
        $("#unit-name").val(this.name);
        this.setName();
        ["editor-name",
         "display-name",
         "rollover",
         "shortrollover"].forEach(field => {
            $(`#unit-${field}`).val(this[dashToSnakeCase(field) as keyof Unit]);
            $(`#unit-${field}-id`).val(this[dashToSnakeCase(`${field}-id`) as keyof Unit]);
            this.setString(`unit-${field}`);
        });
        (["food", "wood", "gold", "trade"] as const).forEach(resource => {
            $(`#unit-${resource}-cost`).val(this[`${resource}_cost` as keyof Unit]);
            this.setCost(resource);
        });
        ["population", "train-time", "los"].forEach(field => {
            $(`#unit-${field}`).val(this[dashToSnakeCase(field) as keyof Unit]);
            this.setNumber(`unit-${field}`);
        });
        $("#unit-limit").val(this.build_limit.toString());
        this.setNumber("unit-limit", "inf");
        $("#unit-speed").val(this.speed.toString());
        this.setNumber("unit-speed", '-', true);

        $("#unit-armor").val(this.armor.toString());
        $("#unit-armor-type").val(this.armor_type);

        $("#unit-age").val(this.age.toString());
        $("#unit-hp").val(this.hp.toString());
        this.setHP();

        (["ranged", "melee", "siege", "cannon"] as const).forEach(attack_type => {
            const attack = this.main_attacks[attack_type];
            $(`#unit-${attack_type}-attack`).val(attack.damage);
            $(`#unit-${attack_type}-rof`).val(attack.rof);
            let attack_element = $(`#unit-${attack_type}`)[0];
            ((attack_element.getElementsByClassName("unit-damagearea")[0]) as HTMLInputElement).value = attack.damagearea.toString();
            ((attack_element.getElementsByClassName("unit-damagecap")[0]) as HTMLInputElement).value = attack.damagecap.toString();
            ((attack_element.getElementsByClassName("unit-range")[0]) as HTMLInputElement).value = attack.range.toString();
            //this.setAttack(attack_type);
        
            attack_element.getElementsByClassName("damage-bonuses")[0].innerHTML = '';
            for (const type in this.main_attacks[attack_type].multipliers) {
                if (this.main_attacks[attack_type].multipliers.hasOwnProperty(type)) {
                    const multiplier = Math.floor(20 * this.main_attacks[attack_type].multipliers[type]) / 20;
                    let bonus_element = 
                    <li>
                        <span class="numeric unit-damage-bonus-type" style="opacity: 85%;">{multiplier}</span>
                        <span style="opacity: 80%;"> vs <span class="attribute-value unit-damage-bonus-type">{type}</span></span>
                    </li>;
                    attack_element.getElementsByClassName("damage-bonuses")[0].appendChild(bonus_element);
                }
            }
        });
        this.getAdjustedAttack();

        this.createXMLEditor()
        this.bindToDocument();

    }

    /**
     * Adds `onclick` and `onchange` type functions for the unit's input fields.
     */
    bindToDocument() {
        bindStrings(string_id_types, this);

        $("#unit-id").on("change", () => { this.setUnitID(); });
        $("#unit-name").on("change", () => { this.setName(); });

        (["food", "gold", "wood", "trade"] as const).forEach(resource => {
            $(`#unit-${resource}-cost`).on("change", () => { this.setCost(resource); });
        });

        $("#unit-age").on("change", () => { this.setAge(); });
        $("#unit-hp").on("change", () => { this.setHP(); });
        $("#unit-adjusted-hp").on("change", () => { this.setAsjustedHP(); });

        $("#unit-population").on("change", () => { this.setNumber("unit-population"); });
        $("#unit-train-time").on("change", () => { this.setNumber("unit-train-time"); });
        $("#unit-limit").on("change", () => { this.setNumber("unit-limit", "inf"); });
        $("#unit-los").on("change", () => { this.setNumber("unit-los"); });

        $("#unit-armor").on("change", () => { this.setArmor(); });
        $("#unit-armor-type").on("change", () => { this.setArmorType(); });
        $("#unit-speed").on("change", () => { this.setSpeed(); });
        //$("#unit-speed").on("change", () => { this.setNumber("unit-speed", '-', true); });

        (["melee", "ranged", "siege", "cannon"] as const).forEach(attack_type => {
            $(`#unit-${attack_type}-attack`).on("change", () => { this.setAttack(attack_type); });
            $(`#unit-${attack_type}-rof`).on("change", () => { this.setAttack(attack_type); });
            $(`#unit-${attack_type} .unit-damagearea`).on("change", () => { this.setAttack(attack_type); });
            $(`#unit-${attack_type} .unit-damagecap`).on("change", () => { this.setAttack(attack_type); });
            $(`#unit-${attack_type} .unit-range`).on("change", () => { this.setAttack(attack_type); });
        });
    }

    /**
     * Update the unit's XML for:
     *  -  buildlimit
     *  -  trainpoints
     *  -  populationcount
     *  -  los
     * Then update the XML editor.
     */
    private updateXML() {
        this.setXMLValue("buildlimit", this.build_limit, "los");
        this.setXMLValue("trainpoints", this.train_time, "los");
        this.setXMLValue("populationcount", this.population, "editornameid");
        this.$xml.find("los")[0].textContent = this.los.toString();
        this.updateXMLEditor();
    }
    /**
     * Add, update or remove an XML tag.
     * @param tag_name Tag name to be added/changed/removed.
     * @param value The new value for the tag. If the tag value is numeric, but NaN, the tag will be removed.
     * @param after_tag An existing tag that the new one will follow if it doesn't exist yet.
     * @param is_string Is the tag value a string, i. e. is a NaN allowed as the value.
     * If not, and the value is `''` or other NaN, the tag will be removed. False by default.
     * @param xml The JQuery XML element to put the tag into. Uses the unit's XML by default.
     */
    private setXMLValue(tag_name: string, value: string|number, after_tag: string, is_string=false, xml=this.$xml) {
        if((!is_string && (isNaN(value as number) || value == 0)) || 
            (is_string && value == '')) {
            xml.find(tag_name).remove();
        }
        else {
            if(xml.find(tag_name).length === 0) {
                let build_limit = xmlDoc.createElement(tag_name);
                build_limit.textContent = value.toString();
                xml.find(after_tag).after(build_limit);
            }
            else
                xml.find(tag_name)[0].textContent = value.toString();
        }
    }
    /**
     * Create a new XML editor for the unit, unless there exists one already
     * or the user preferences have XML Editing off.
     * @param initial_value The text put into the editor. Uses the unit's XML by default.
     */
    createXMLEditor(initial_value:string = prettyPrintXML(this.$xml[0].outerHTML)) {
        if(!preferences["xml-editing"])
            return;
        $("#unit-xml").css("display", "block");
        $("#unit-xml textarea").html('');
        $("#unit-xml textarea").text(initial_value);
        this.unit_xml_editor = CodeMirror.fromTextArea($(`#unit-xml textarea`)[0], {
            value: initial_value,
            //keyMap: 'sublime', // Bugged rn
            lineNumbers: true,
            mode: "text/xml",
            indentWithTabs: false,
            readOnly: !preferences["live-xml"]
        });
    }
    /**
     * Replace the text in the XML editor with the one provided.
     * @param value The new text to be put into the editor. Uses the unit's XML by default.
     */
    updateXMLEditor(value:string = prettyPrintXML(this.$xml[0].outerHTML)) {
        if(this.unit_xml_editor != undefined && preferences["xml-editing"])
            this.unit_xml_editor.setValue(value.replace(/ xmlns=\"(.*?)\"/g, ''));
    }

    /**
     * Run through the techtree and find the unit upgrades.
     * Then asynchronously initialize them and render them to the DOM.
     * @param name Name of the unit to find upgrades for. `this.name` by default.
     */
    private async getUpgrades(name = this.name) {
        $("#unit-upgrades").html('');
        let upgrades = $techs.find(`tech:contains("UpgradeTech")>effects>effect[subtype="Hitpoints"]>target[type="ProtoUnit"]:contains("${name}")`).filter( 
            function () { return ($(this).text() == name);}
        ).parents("tech").not('[name^="HC"]');
        this.upgrades = {};
        for (const u of upgrades as any) {
            this.addUpgrade(u);
        }
        for (const tech_element in this.upgrades) {
            if (this.upgrades.hasOwnProperty(tech_element)) {
                this.upgrades[tech_element].bindToDocument();
                this.upgrades[tech_element].createXMLEditor();
            }
        }
    }
    /**
     * Asynchronously add a unit upgrade to the list and render it.
     * @param $upgrade An upgrade's XML.
     */
    private async addUpgrade($upgrade: HTMLElement) {
        let upgrade = new Tech($upgrade);
        this.upgrades[upgrade.element_name] = upgrade;
        $("#unit-upgrades")[0].appendChild(upgrade.render());
        // The age dropdown doesn't work inside the JSX, so we do this
        $(`#${upgrade.element_name}-age`).val(current_unit.upgrades[upgrade.element_name].age);
    }

    /**
     * Asynchronously run through the proto and find where the unit can be trained at.
     * The list goes to `unit.buildings`.
     * @param name Name of the unit to find buildings for. `this.name` by default.
     */
    private async getBuildings(name = this.name) {
        let buildings:Record<string, Building> = {};
        $proto.find(`train:contains("${name}")`).map(function () {
            buildings[$(this).parent().attr("name")] = { 
                name: $(this).parent().attr("name"),
                is_main: false,
                column: parseInt($(this).attr("column"))
             };
        });
        this.buildings = Object.values(buildings);
    }
    /**
     * Find buildings that not only train the unit, but also upgrade it.
     */
    private async getMainBuildings(upgrades:string[] = Object.keys(this.upgrades), buildings:Building[] = this.buildings) {
        console.log(upgrades);
        upgrades.forEach(upgrade => {
            for (let index = 0; index < buildings.length; index++) {
                $proto.find(`unit[name="${buildings[index].name}"] tech:contains("${dashToCamelCase(upgrade)}")`).parent().map(function () {
                    buildings[index].is_main = true;
                });
            }
        });
        this.buildings = buildings;
        console.log(buildings);
    }
    /**
     * Display the buildings te unit can be trained at.
     * @param buildings A record of buildings to display. Takes the unit's by default.
     */
    private renderAllBuildings(buildings:Building[] = this.buildings) {
        $("#unit-building-list").empty();
        for (let index = 0; index < buildings.length; index++) {
            const building = buildings[index];
            this.renderBuilding(index, building);
        }
        this.updateMasterBuilding(buildings);
    }
    /**
     * Add a building to the list in the DOM and bind onclicks in it.
     * @param index Building's number.
     * @param building A building object to add.
     */
    private renderBuilding(index: number, building: Building) {
        $("#unit-building-list")[0].appendChild(
            <tr id={`unit-building-${index}`}>
                <td>
                    <input list="units" class="unit-building-name" type="text" autocomplete="on" value={building.name} />
                </td>
                <td class="tag">
                    column:
                    <input id={`unit-building-${index}-column`} class="numeric double-digit" type="number" min="0" max="30" value={building.column} />
                </td>
                <td>
                    <span style="display: flex; justify-content: center;">
                        <label class="toggle" style="margin-top: 0.25rem;">
                            <input id={`unit-building-${index}-is-main`} type="checkbox" checked={building.is_main} />
                            <span class="slider"></span>
                        </label>
                        <span class="comment">upgrades</span>
                    </span>
                </td>
                <td>
                    <button id={`unit-building-${index}-delete-button`} class="building-delete-button">
                        <svg viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z" /><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm0-9.414l2.828-2.829 1.415 1.415L13.414 12l2.829 2.828-1.415 1.415L12 13.414l-2.828 2.829-1.415-1.415L10.586 12 7.757 9.172l1.415-1.415L12 10.586z" /></svg>
                    </button>
                </td>
            </tr>
        );
        $(`#unit-building-${index}-delete-button`)[0].innerHTML += ' ';
        $(`#unit-building-${index}-delete-button`).on("click", function () {
            delete current_unit.buildings[parseInt(this.id.split('-')[2])];
            $(this).parent().parent().remove();
            current_unit.updateMasterBuilding();
        });
        $(`#unit-building-${index} input.unit-building-name`).on("blur", function () {
            if((this as HTMLInputElement).value == '')
                $(this.parentElement.parentElement).find("button.building-delete-button").trigger("click");
            else {
                current_unit.buildings[parseInt(this.parentElement.parentElement.id.split('-')[2])].name = (this as HTMLInputElement).value;
                current_unit.updateMasterBuilding();
            }
        });
        $(`#unit-building-${index}-column`).on("change", function () {
            current_unit.buildings[parseInt(this.id.split('-')[2])].column = parseInt((this as HTMLInputElement).value);
        });
        $(`#unit-building-${index}-is-main`).on("change", function () {
            current_unit.buildings[parseInt(this.id.split('-')[2])].is_main = (this as HTMLInputElement).checked;
            current_unit.updateMasterBuilding();
        });
    }
    /**
     * Update the `#unit-main-building` header.
     * @param buildings A record of buildings to display. Takes the unit's by default.
     */
    private updateMasterBuilding(buildings:Building[] = this.buildings) {
        let master_building_name = "no buildings yet";
        for (let index = 0; index < buildings.length; index++) {
            if(buildings[index] !== undefined) {
                const building = buildings[index];
                //console.log(`Master name: ${master_building_name}`);
                //console.log(`Current option: ${building.name}`);
                if(master_building_name === "no buildings yet" || building.is_main) {
                    master_building_name = getString($proto.find(`unit[name="${building.name}"]>displaynameid`).text());
                    if(building.is_main)
                        break;
                }
            }
        }
        $("#unit-main-building").text(master_building_name);
    }
    /**
     * Add a new building, initially empty
     */
    addEmptyBuilding() {
        let column = $(`input.numeric.double-digit`)?.val()?.[0] || 0;
        let new_building = { 
            name: '',
            is_main: true,
            column: parseInt(column)
        };
        this.buildings.push(new_building);
        this.renderBuilding(this.buildings.length - 1, new_building);
        $("input.unit-building-name").last().trigger("focus");
    }

    /**
     * Change the unit's resource cost from the corresponding DOM's input field; then recalculate cost efficiency, total cost, bounty, buildbounty.
     * @param resource The resource to change cost in, such as `gold` or `trade`.
     */
    setCost(resource: Resource, new_cost: number = parseInt($(`#unit-${resource}-cost`).val() as string)) {
        this[`${resource}_cost` as `${Resource}_cost`] = new_cost;
        this.total_cost = Math.round(10 * this.population + this.food_cost + this.wood_cost + this.gold_cost + 400 * this.trade_cost / 290);
        if (this[`${resource}_cost` as `${Resource}_cost`] > 0) {
            $(`#unit-${resource.toLowerCase()}-tag`).css("opacity", 0.9);
            $(`#unit-${resource.toLowerCase()}`).css("opacity", 0.9);
        }
        else {
            $(`#unit-${resource.toLowerCase()}-tag`).css("opacity", 0.5);
            $(`#unit-${resource.toLowerCase()}`).css("opacity", 0.5);
        }
        this.getEfficiency();
        $("#unit-total-cost").text(this.total_cost.toString());
        let resource_upper = resource.slice(0, 1).toUpperCase() + resource.slice(1);
        if(this[`${resource}_cost` as `${Resource}_cost`] > 0 && this.$xml.find(`cost[resourcetype="${resource_upper}"]`).length === 0)
            this.$xml.find("buildbounty").after(<cost resourcetype={resource_upper}>{this[`${resource}_cost` as `${Resource}_cost`]}</cost>);
        else
            this.setXMLValue(`cost[resourcetype="${resource_upper}"]`, this[`${resource}_cost` as `${Resource}_cost`], "buildbounty");
        this.setXMLValue("bounty", Math.floor((this.food_cost+this.wood_cost+this.gold_cost+this.trade_cost + 5) / 10), "unitaitype");
        this.setXMLValue("buildbounty", Math.floor((this.food_cost+this.wood_cost+this.gold_cost+this.trade_cost + 5) / 10), "bounty");
        this.updateXMLEditor();
    }
    /**
     * Set a new unit ID and check if it is unique.
     * @param new_id The new unit ID. Reads one from the DOM in `#unit-id` by default.
     */
    setUnitID(new_id:number = parseInt($("#unit-id").val() as string)) {
        this.id = new_id;
        if ($proto.find(`unit[id=${this.id}]`).length > 0)
            $("#unit-id-warning").addClass("critical");
        else
            $("#unit-id-warning").removeClass("critical")
        this.$xml.attr("id", this.id)
        this.updateXMLEditor();
    }
    /**
     * Set a new unit DBID.
     * @param new_id The new unit DBID.
     */
    setUnitDBID(new_dbid:number) {
        this.setXMLValue("dbid", new_dbid, "displaynameid");
        this.updateXMLEditor();
    }
    /**
     * Set a new unit name and check if it is unique. Update file paths and upgrade names accordingly 
     * (unless the new name is the same).
     * @param new_name The new unit name. Reads one from the DOM in `#unit-name` by default.
     */
    setName(new_name:string = $("#unit-name").val() as string) {
        const old_name = this.name;
        this.name = new_name;
        if ($proto.find(`unit[name="${this.name}"]`).length > 0)
            $("#unit-name-warning").addClass("critical");
        else
            $("#unit-name-warning").removeClass("critical");
        if(old_name === new_name)
            return;
        let trimmed_name = this.getTrimmedName();
        this.$xml.find("animfile")[0].textContent = this.file_path_template.replace(/#T#/g, trimmed_name) + trimmed_name + ".xml";
        this.setXMLValue("icon", this.file_path_template.replace(/#T#/g, trimmed_name) + trimmed_name + "_icon_128x128", "animfile", true);
        this.setXMLValue("portraiticon", this.file_path_template.replace(/#T#/g, trimmed_name) + trimmed_name + "_portrait", "animfile", true);
        this.$xml.attr("name", this.name);
        this.updateXMLEditor();
        for (const key in this.upgrades) {
            if (Object.prototype.hasOwnProperty.call(this.upgrades, key)) {
                const upgrade = this.upgrades[key];
                upgrade.setName($(`#${upgrade.element_name}-name`).val() as string, old_name);
            }
        }
    }
    /**
     * Set a new unit allowed age.
     * @param new_age The new unit's allowed age. Reads one from the DOM in `#unit-age` by default.
     */
    setAge(new_age:number = parseInt($("#unit-age").val() as string)) {
        this.age = new_age;
        this.$xml.find("allowedage")[0].textContent = this.age.toString();
        this.updateXMLEditor();
    }
    /**
     * Change a numeric unit property, such as `train_time`, to the one found in the corresponding DOM's input field.
     * @param property_name Which property of the unit's object is to be changed. **Should be written in`dash-case`**.
     * @param null_value What should zero/NaN value be replaced with, for example, `"inf"` or `1`. Default value is `0`.
     * @param is_float Are float values allowed. False by default.
     */
    setNumber(property_name: string, null_value:string = '0', is_float=false) {
        let property_name_snake_case = property_name.slice(5).replace(/-/g, '_') as "total_cost"|"population"|"train_time"|"build_limit"|"los"|"adjusted_attack"|"specialization";
        if (is_float)
            this[property_name_snake_case] = parseFloat($(`#${property_name}`).val() as string);
        else
            this[property_name_snake_case] = parseInt($(`#${property_name}`).val() as string);
        if (isNaN(this[property_name_snake_case]) || this[property_name_snake_case] == 0) {
            this[property_name_snake_case] = 0;
            $(`#${property_name}`).css("opacity", 0.5);
            $(`#${property_name}-tag`).css("opacity", 0.5);
            if (typeof(null_value) == "string")
                ($(`#${property_name}`)[0] as HTMLInputElement).type = "text";
            $(`#${property_name}`).val(null_value);
        }
        else {
            $(`#${property_name}`).css("opacity", 0.9);
            $(`#${property_name}-tag`).css("opacity", 0.9);
            ($(`#${property_name}`)[0] as HTMLInputElement).type = "number";
        }
        this.total_cost = Math.round(10 * this.population + this.food_cost + this.wood_cost + this.gold_cost + 400 * this.trade_cost / 290);
        this.updateXML();
        this.getEfficiency();
    }
    /**
     * Change a string and its stringtable ID in a unit.
     * @param string_name The string property of the unit's object to change, like `unit-display-name`. 
     * @param new_string New string value. Gets one from DOM by default.
     * **Should be written in `dash-case` and include the word "unit"**.
     */
    setString(string_name: string, new_string: string = undefined) {
        let string_type = dashToSnakeCase(string_name);
        if(string_type.startsWith("unit_"))
            string_type = string_type.slice(5) as UnitStringType;
        const id_type = string_type + "_id" as `${UnitStringType}_id`;
        const new_id = parseInt($(`#${string_name}-id`).val() as string)
        const old_string = this[string_type];
        const old_id = this[id_type];
        if(new_string === undefined)
            new_string = $(`#${string_name}`).val() as string;
        this[string_type] = new_string;
        this[id_type] = new_id;

        if (string_name == "unit-display-name" && new_string !== old_string) {
            $("#unit-main-name").text(this.display_name);
            for (const upgrade_name in this.upgrades) {
                if (Object.prototype.hasOwnProperty.call(this.upgrades, upgrade_name)) {
                    const upgrade = this.upgrades[upgrade_name];
                    upgrade.updateTemplate(`${upgrade_name}-rollover`, old_string);
                    upgrade.updateTemplate(`${upgrade_name}-display-name`, old_string);
                    upgrade.updateTemplate(`${upgrade_name}-new-name`, old_string);
                }
            }
            if(this.editor_name === this.editor_name_template.replace(/#T#/g, old_string)) {
                $("#unit-editor-name").val(this.editor_name_template.replace(/#T#/g, this.display_name));
                this.setString("unit-editor-name", this.editor_name_template.replace(/#T#/g, this.display_name));
            }
        }

        updateStringRecord(string_name, {new_id: new_id, new_value: new_string, old_id: old_id, old_value: old_string});

        let tag_name = string_type.replace(/_/g, '').replace("rollover", "rollovertext") + "id" as `${"displayname"|"editorname"|"rollovertext"|"shortrollovertext"}id`;
        let prev_tag = {displaynameid: "dbid", editornameid: "displaynameid", rollovertextid: "editornameid", shortrollovertextid: "maxhitpoints"}[tag_name];
        this.setXMLValue(tag_name, new_id, prev_tag);
        this.updateXMLEditor();
    }
    /**
     * Set a new unit HP. Unit adjusted HP, cost efficiency and XML are updated accordingly.
     * @param new_hp The new unit HP. Reads one from the DOM in `#unit-hp` by default.
     */
    setHP(new_hp:number = parseInt($("#unit-hp").val() as string)) {
        this.hp = new_hp;
        this.getAdjustedHP(new_hp);
        $("#unit-adjusted-hp").val(this.adjusted_hp);
        this.$xml.find("maxhitpoints").text(this.hp);
        this.$xml.find("initialhitpoints").text(this.hp);
        this.updateXMLEditor();
        this.getEfficiency();
    }
    /**
     * Set a new adjusted unit HP. Unit HP, cost efficiency and XML are updated accordingly.
     * @param new_adjusted_hp The new unit HP. Reads one from the DOM in `#unit-adjusted-hp` by default.
     */
    setAsjustedHP(new_adjusted_hp:number = parseInt($("#unit-adjusted-hp").val() as string)) {
        this.adjusted_hp = new_adjusted_hp;
        this.hp = Math.floor(this.adjusted_hp * (1 - this.armor / 2.5));
        $("#unit-hp").val(this.hp);
        this.$xml.find("maxhitpoints").text(this.hp);
        this.$xml.find("initialhitpoints").text(this.hp);
        this.updateXMLEditor();
        this.getEfficiency();
    }
    /**
     * Set a new unit speed. Unit's run speed, cost efficiency and XML are updated accordingly.
     * @param new_speed The new unit speed. Reads one from the DOM in `#unit-speed` by default.
     * @param run_speed The new unit maximum (running) speed. Adds 2 to the `new_speed` by default.
     */
    setSpeed(new_speed = parseFloat($("#unit-speed").val() as string), run_speed = 2.0 + new_speed) {
        this.speed = new_speed;
        this.$xml.find("maxvelocity").text(new_speed);
        this.$xml.find("maxrunvelocity").text(run_speed);
        this.updateXMLEditor();
        this.getEfficiency();
    }
    /**
     * Set a new unit armor type. Unit's XML updated accordingly.
     * @param armor_type The new armor type: "Ranged", "Hand" or "Siege". Reads one from the DOM in `#unit-armor-type` by default.
     */
    setArmorType(armor_type:ArmorType = $("#unit-armor-type").val() as ArmorType) {
        this.armor_type = armor_type;
        this.$xml.find("armor")[0].setAttribute("type", this.armor_type);
        this.updateXMLEditor();
    }
    /**
     * Set a new unit armor amount. Unit adjusted HP, cost efficiency and XML are updated accordingly.
     */
    setArmor() {            
        this.setNumber("unit-armor", '0', true);
        this.getAdjustedHP();
        $("#unit-adjusted-hp").val(this.adjusted_hp)
        this.$xml.find("armor")[0].setAttribute("value", this.armor.toString());
        this.updateXMLEditor();
        this.getEfficiency();
    }
    /**
     * Update the unit's attack of a specific type with values from the DOM. 
     * Then modify the protoactions in the unit's XML and update 
     * adjusted attack and, therefore, cost efficiency.
     * @param attack_type An attack type to update.
     */
    setAttack(attack_type: AttackType) {
        const element = $(`#unit-${attack_type}`);

        this.main_attacks[attack_type].damage = parseFloat($(`#unit-${attack_type}-attack`).val() as string);
        this.main_attacks[attack_type].rof = parseFloat($(`#unit-${attack_type}-rof`).val() as string);
        this.main_attacks[attack_type].damagearea = parseFloat(element.find(".unit-damagearea").val() as string);
        this.main_attacks[attack_type].damagecap = parseFloat(element.find(".unit-damagecap").val() as string);
        this.main_attacks[attack_type].range = parseFloat(element.find(".unit-range").val() as string);
        
        element.find(".damage-bonuses").html('');
        for (const type in this.main_attacks[attack_type].multipliers) {
            if (this.main_attacks[attack_type].multipliers.hasOwnProperty(type)) {
                const multiplier = Math.floor(20 * this.main_attacks[attack_type].multipliers[type]) / 20;
                let bonus_element = 
                <li>
                    <span class="numeric unit-damage-bonus-type" style="opacity: 85%;">{multiplier}</span>
                    <span style="opacity: 80%;"> vs <span class="attribute-value unit-damage-bonus-type">{type}</span></span>
                </li>;
                element.find(".damage-bonuses")[0].appendChild(bonus_element);
            }
        }

        if(this.main_attacks[attack_type].damagearea == 0 || this.main_attacks[attack_type].damagecap == 0)
            element.find(".area-damage").css("opacity", 0.4);
        else
            element.find(".area-damage").css("opacity", 0.75);
        if(this.main_attacks[attack_type].range == 0)
            element.find(".attack-range").css("opacity", 0.5);
        else
            element.find(".attack-range").css("opacity", 0.9);

        const actions = this.$xml.find("protoaction");
        for (const key in actions) {
            if (actions.hasOwnProperty(key) && typeof(actions[key]) == "object") {
                const action = actions[key];
                try {
                    const name = action.getElementsByTagName("name")[0].textContent;
                    let type;
                    for (const action_type in action_types)
                        if (action_types.hasOwnProperty(action_type) && name.endsWith(action_type)) {
                            type = action_types[action_type as keyof typeof action_types];
                            break;
                        }
                    if(attack_type == type) {
                        action.getElementsByTagName("damage")[0].textContent = this.main_attacks[type].damage.toString();
                        action.getElementsByTagName("rof")[0].textContent = this.main_attacks[type].rof.toString();
                        this.setXMLValue("maxrange", this.main_attacks[type].range, "damage", false, $(action));
                        if(this.main_attacks[type].damagecap * this.main_attacks[type].damagearea == 0 || name.startsWith("Guardian")) {
                            this.setXMLValue("damagecap", 0, "rof", false, $(action));
                            this.setXMLValue("damagearea", 0, "rof", false, $(action));
                        }
                        else {
                            this.setXMLValue("damagecap", this.main_attacks[type].damagecap, "rof", false, $(action));
                            this.setXMLValue("damagearea", this.main_attacks[type].damagearea, "rof", false, $(action));
                            if(action.getElementsByTagName("damageflags")[0]?.textContent == undefined)
                                this.setXMLValue("damageflags", "Enemy", "damagearea", false, $(action));
                        }
                        if(name.startsWith("Trample")) {
                            action.getElementsByTagName("damage")[0].textContent = (this.main_attacks[type].damage * 2 / 3).toString();
                            action.getElementsByTagName("rof")[0].textContent = (this.main_attacks[type].rof * 4 / 3).toString();
                            this.setXMLValue("damagecap", 2, "rof", false, $(action));
                            this.setXMLValue("damagearea", this.main_attacks[type].damage * 2, "rof", false, $(action));
                            this.setXMLValue("damageflags", "Enemy", "damagearea", false, $(action));
                        }
                    }
/*
                    if (action.getElementsByTagName("maxrange").length)
                        protoaction["range"] = parseFloat(action.getElementsByTagName("maxrange")[0].textContent);
                    if (action.getElementsByTagName("damagearea").length) {
                        protoaction["damagearea"] = parseFloat(action.getElementsByTagName("damagearea")[0].textContent);
                        protoaction["damagecap"] = parseFloat(action.getElementsByTagName("damagecap")[0].textContent);
                    }
*/
                } catch(e) { console.log(e); }
            }
        }

        this.getAdjustedAttack();
        this.updateXMLEditor();

    }
}


$("#proto-selector").on("click", selectProtoFile);
$("#strings-selector").on("click", selectStringsFile);
$("#techs-selector").on("click", selectTechsFile);
$("#multiple-files-selector").on("click", selectMultipleFiles);

$("#proto-selector-out").on("click", function () {selectProtoFile(null, "save")});
$("#strings-selector-out").on("click", function () {selectStringsFile(null, "save")});
$("#techs-selector-out").on("click", function () {selectTechsFile(null, "save")});
$("#multiple-saving-selector").on("click", function () {selectMultipleFiles(null, "save")});

const singleFileDialog:OpenDialogOptions = {
    properties: ["openFile"], filters: [
        { name: 'XML data files', extensions: ['xml'] },
        { name: 'All files', extensions: ['*'] }
    ]
};
const multipleFileDialog:OpenDialogOptions = {
    properties: ["openFile", "multiSelections"], filters: [
        { name: 'XML data files', extensions: ['xml'] },
        { name: 'All files', extensions: ['*'] }
    ]
};
/**
 * Open an OS file select dialog to open/save the proto file.
 * Then fill in the corresponding DOM input with the file path.
 * @param event Click or other event that triggered this function. Unused.
 * @param mode Whether to select a file for loading or for saving. `"open"` by default.
 * @param callback Function to execute after the file has been selected. 
 * Just prints the file path by default.
 */
async function selectProtoFile(
        event = null, 
        mode: "open"|"save" = "open", 
        callback = (files: { filePaths: any[]; }) => {console.log(files.filePaths[0]);}) {
    dialog.showOpenDialog(singleFileDialog).then((files) => {
        callback(files);
        if(files.filePaths[0] !== undefined) {
            switch (mode) {
                case "open":
                    $("#proto-filepath").val(files.filePaths[0]);
                case "save":
                    $("#proto-filepath-out").val(files.filePaths[0]);
                default:
                    break;
            }
        }
    });
}
/**
 * Open an OS file select dialog to open/save the stringtable file.
 * Then fill in the corresponding DOM input with the file path.
 * @param event Click or other event that triggered this function. Unused.
 * @param mode Whether to select a file for loading or for saving. `"open"` by default.
 * @param callback Function to execute after the file has been selected. 
 * Just prints the file path by default.
 */
async function selectStringsFile(
        event = null, 
        mode: "open"|"save" = "open", 
        callback = (files: { filePaths: any[]; }) => {console.log(files.filePaths[0]);}) {
    dialog.showOpenDialog(singleFileDialog).then((files) => {
        callback(files);
        if(files.filePaths[0] !== undefined) {
            switch (mode) {
                case "open":
                    $("#strings-filepath").val(files.filePaths[0]);
                case "save":
                    $("#strings-filepath-out").val(files.filePaths[0]);
                default:
                    break;
            }
        }
    });
}
/**
 * Open an OS file select dialog to open/save the techtree file.
 * Then fill in the corresponding DOM input with the file path.
 * @param event Click or other event that triggered this function. Unused.
 * @param mode Whether to select a file for loading or for saving. `"open"` by default.
 * @param callback Function to execute after the file has been selected. 
 * Just prints the file path by default.
 */
async function selectTechsFile(
        event = null, 
        mode: "open"|"save" = "open", 
        callback = (files: { filePaths: any[]; }) => {console.log(files.filePaths[0]);}) {
    dialog.showOpenDialog(singleFileDialog).then((files) => {
        callback(files);
        if(files.filePaths[0] !== undefined) {
            switch (mode) {
                case "open":
                    $("#techs-filepath").val(files.filePaths[0]);
                case "save":
                    $("#techs-filepath-out").val(files.filePaths[0]);
                default:
                    break;
            }
        }
    });
}
/**
 * Open an OS file select dialog to open several XML files.
 * Then fill in the corresponding DOM inputs with the file paths.
 * @param event Click or other event that triggered this function. Unused.
 * @param mode Whether to select a file for loading or for saving. `"open"` by default.
 * @param callback Function to execute after the file has been selected. 
 * Just prints the file list by default.
 */
 async function selectMultipleFiles(
    event = null, 
    mode: "open"|"save" = "open", 
    callback = (files: { filePaths: any[]; }) => {console.log(files.filePaths[0]);}) {
    dialog.showOpenDialog(multipleFileDialog).then((files) => {
        callback(files);
        switch (mode) {
            case "open":
                files.filePaths.forEach(file => {
                    switch (file.split('\\').pop().substr(0, 4)) {
                        case "prot":
                            $("#proto-filepath").val(file);
                            break;
                        case "tech":
                            $("#techs-filepath").val(file);
                            break;
                        case "stri":
                            $("#strings-filepath").val(file);
                            break;
                        default:
                            break;
                    }
                });
            case "save":
                files.filePaths.forEach(file => {
                    switch (file.split('\\').pop().substr(0, 4)) {
                        case "prot":
                            $("#proto-filepath-out").val(file);
                            break;
                        case "tech":
                            $("#techs-filepath-out").val(file);
                            break;
                        case "stri":
                            $("#strings-filepath-out").val(file);
                            break;
                        default:
                            break;
                    }
                });
            default:
                break;
        }
    });
}

/**
 * Update the new file paths in user preferences and load these files. 
 * Display loading screen in the process.
 * @param event Click or other event that triggered this function. Unused.
 */
function applyFileSelection(event = null) {
    $("#loading-container").css("display", "flex");
    $("#files-settings-container").css("display", "none");
    preferences["proto-file"] = $("#proto-filepath").val() as string;
    preferences["techtree-file"] = $("#techs-filepath").val() as string;
    preferences["stringtable-file"] = $("#strings-filepath").val() as string;
    setTimeout(function () {
        savePreferences();
        loadFiles();
        $("#loading-container").css("display", "none");
    }, 100);
}

$("#apply-file-selection").on("click", applyFileSelection);
$("#save").on("click", saveAll);


/**
 * Trigger saving all the changes and toggling the loading screen
 */
function saveAll() {
    $("#loading-container").css("display", "flex");
    $("#saving-settings-container").css("display", "none");
    preferences["proto-out"] = $("#proto-filepath-out").val() as string;
    preferences["techtree-out"] = $("#techs-filepath-out").val() as string;
    preferences["stringtable-out"] = $("#strings-filepath-out").val() as string;
    savePreferences();
    setTimeout(function () {
        saveChanges();
        $("#loading-container").css("display", "none");
    }, 50);
}

/**
 * Load a unit and its upgrades from the XMLs. Display the loading screen in the process.
 * @param event An event that triggered the call. Unused.
 * @param base_unit The unit's name. Takes one from the DOM's `"#select-base-unit"` by default.
 */
function loadUnit(event:Event = null, base_unit:string = $("#select-base-unit").val() as string) {

    $("#loading-container").css("display", "flex");
    $("#unit-xml div.CodeMirror").remove();
    let baseXML = $proto.find(`unit[name=${base_unit}]`);

    if(baseXML.length > 0) {
        setTimeout(function() {
            string_id_types = [
                "unit-display-name",
                "unit-editor-name",
                "unit-rollover",
                "unit-shortrollover",
            ];
            new_strings_record = {};
            current_unit = new Unit(baseXML.clone());
            console.log(current_unit);
            current_unit.render();
        }, 0);
        setTimeout(function() {
            $("#loading-container")[0].style.display = "none";
        }, 750);
    }
    else
        $("#loading-container")[0].style.display = "none";
    
    return false;
}

