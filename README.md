# Unit Editor for Age of Empires III


## What is this?

This is a GUI tool aimed at streamlining creating and editing units while modding Age of Empires III. It is supposed to help with mundane tasks such as copying the bulk of a unit's XML, creating unit upgrades and editing a unit's combat stats in a well-balanced way. It is written as a web app with *Electron*, which allows it to be packed into an .EXE or even to be built for Mac/Linux.

![Editor demo](demos/screenshot1.jpg?raw=true)
![Editor demo](demos/screenshot2.jpg?raw=true)
![Editor demo](demos/screenshot3.jpg?raw=true)

## How do I use it?

### Installation

You can get the compiled Windows version in the `out` folder. No installation is required in this case, just download the folder as ZIP, unpack and you can simply launch the .EXE inside.

If you're reading this as a developer, you'll probably know how to build this project using *Electron Forge* or other similar utilities. This project uses *Electron Forge* and is built to `out` by running the `npm run package` script. To run the source code, run `npm run start` for building and then running the app or `npm run debug` to run a build with a Typescript watcher to recompile automatically on changes. Don't forget to install the dependencies with `npm install`.

### Selecting XMLs

When you launch the editor, it will try to open the files you edited during the last session. At first launch it will try opening the `protoy.xml`, `techtreey.xml`, `stringtabley.xml` files from it's home folder, so you can copy the files you want to edit into the same directory as *Unit Editor*. Otherwise it will open a page to select the three .XML files.

To open other files, hover the *Settings* icon on the bottom right and press the folder icon. You can also go to *Files > Open XMLs* or hit `Ctrl+O` to open all three files at once. The file paths you selected will be saved to your preferences.

XML files **must** be in `UTF-8` encoding.

### Creating a unit

In the box on the top right, input a base unit for the editor to copy or edit. For example, if you want to make an American archer unit, you can copy an *Aenna*. The more similar the base unit is, the less changes you will have to make.
After that you can edit the unit's stats. If you turn on *Show XML* in the *XML preferencies* section of the bottom right menu, you can also see the new unit's XML, ~~and with *Live editing* - directly edit it~~.
For a new unit, don't forget to rename it and its upgrades, change its ID and in-game names/rollovers.

### Saving

After you've finished modding your unit, click the *Save* icon in the bottom right menu. Select the files to overwrite or input your own file names, then press *Apply*. This will add the unit, its upgrade techs and the related strings to the three .XML files specified.
If a unit or its upgrade name isn't changed (it will be highlighted red), the old one will be replaced rather than copied.
Of course, another option is to manually copy the unit's XML and use it instead. This can be handy if you only want to manually tweak and balance an existing unit without changing its upgrades and names.

### Deinstallation

To remove the app, just delete the folder you downloaded.
To remove user preferences and app cache as well, delete `C:\Users\[username]\AppData\Roaming\unit-editor`.


## Limitations

### Not finished just yet

Some features are being developed right now:
- *Live editing* option is disabled right now: editing is always turned off (even if *Show XML* is turned on).
- Editing attack multipliers: bonus damage can only be edited manually ~~in the XML editor~~ (or rather will be after *Live editing* is implemented).
- Manually editing build/kill XP bounty. It is set automatically based on cost.
- Adding new attacks: you can't add a non-existent attack type, like a cannon attack to a musketeer, **despite the editor allowing you to edit the input fields**.

### Known issues

Some things are just limitations and bugs that are yet to be fixed:
- Bottom-right menu buttons can't be clicked over the XML editor.
- Some zero-valued input fields aren't dimmed consistently.
- Encodings other than `UTF-8` are not supported.

## FAQ

#### Will this work with DE?
There are no guarantees, but provided DE's XML structure stays the same, it should work.

#### I can't select my files.
Make sure your XMLs are in `UTF-8` encoding and have correct structure.

### Does the resulting XML keep comments?
Yes, they are kept intact, including the unit you're copying.

#### The app only shows empty screen on launch.
This is likely some unusual Electron behavior. Try focusing the window by minimizing it and opening back, resizing or pressing any button in the top menu bar.
If this doesn't help, please, report this as a bug.

#### I've found a bug. How can I report it?
Please open an issue [here on GitLab](https://gitlab.com/KEremey/unit-editor/-/issues/new) or contact the creator on [moddb](https://www.moddb.com/members/keremey57).